package com.rubiainteractive.tournamentmanager

import android.app.Activity
import android.app.Application

import com.rubiainteractive.tournamentmanager.di.components.DaggerApplicationComponent
import com.rubiainteractive.tournamentmanager.di.modules.system.ContextModule

import javax.inject.Inject

import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector

class BaseApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        DaggerApplicationComponent
                .builder()
                .setContextModule(ContextModule(applicationContext))
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }
}
