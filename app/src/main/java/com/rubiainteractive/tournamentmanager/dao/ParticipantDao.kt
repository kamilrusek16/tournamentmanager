package com.rubiainteractive.tournamentmanager.dao


import com.rubiainteractive.tournamentmanager.dao.contract.IParticipantDao
import com.rubiainteractive.tournamentmanager.dao.roomdao.ParticipantRoomDao
import com.rubiainteractive.tournamentmanager.db.AppDatabase
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant
import io.reactivex.Maybe
import javax.inject.Inject

class ParticipantDao @Inject
constructor(appDatabase: AppDatabase) : IParticipantDao {

    private val participantRoomDao: ParticipantRoomDao = appDatabase.participantRoomDao

    override val allParticipants: Maybe<List<Participant>>
        get() = participantRoomDao.allParticipants

}
