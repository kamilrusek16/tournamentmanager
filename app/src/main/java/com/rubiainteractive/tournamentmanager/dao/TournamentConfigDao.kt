package com.rubiainteractive.tournamentmanager.dao

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentConfigDao
import com.rubiainteractive.tournamentmanager.dao.roomdao.TournamentConfigRoomDao
import com.rubiainteractive.tournamentmanager.db.AppDatabase
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper

import javax.inject.Inject

class TournamentConfigDao @Inject
internal constructor(appDatabase: AppDatabase) : ITournamentConfigDao {

    private val tournamentConfigRoomDao: TournamentConfigRoomDao = appDatabase.tournamentConfigRoomDao

    override fun getTournamentConfig(typeId: Int): List<TournamentRulesConfigWrapper> {
        return tournamentConfigRoomDao.getTournamentConfig(typeId)
    }

    override fun storeTournamentLeagueRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper) {
        tournamentConfigRoomDao.storeTournamentLeagueRulesConfig(tournamentRulesConfigWrapper)
    }

    override fun storeTournamentCupRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper) {
        tournamentConfigRoomDao.storeTournamentCupRulesConfig(tournamentRulesConfigWrapper)
    }
}
