package com.rubiainteractive.tournamentmanager.dao

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao
import com.rubiainteractive.tournamentmanager.dao.roomdao.TournamentRoomDao
import com.rubiainteractive.tournamentmanager.db.AppDatabase
import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament

import javax.inject.Inject

import io.reactivex.Maybe

class TournamentDao @Inject
constructor(database: AppDatabase) : ITournamentDao {

    private val tournamentRoomDao: TournamentRoomDao = database.tournamentRoomDao

    override val activeTournaments: Maybe<List<Tournament>>
        get() = tournamentRoomDao.activeTournaments

    override fun oneHistoryTournamentRow(): Maybe<Boolean> {
        return tournamentRoomDao.oneHistoryTournamentRow()
                .flatMap { tournaments -> Maybe.just(tournaments.isEmpty()) }
    }
}
