package com.rubiainteractive.tournamentmanager.dao.contract

import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant

import io.reactivex.Maybe

interface IParticipantDao {

    val allParticipants: Maybe<List<Participant>>
}
