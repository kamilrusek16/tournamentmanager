package com.rubiainteractive.tournamentmanager.dao.contract

import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper

interface ITournamentConfigDao {

    fun getTournamentConfig(typeId: Int): List<TournamentRulesConfigWrapper>
    fun storeTournamentLeagueRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper)
    fun storeTournamentCupRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper)
}
