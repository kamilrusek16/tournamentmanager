package com.rubiainteractive.tournamentmanager.dao.contract

import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament

import io.reactivex.Maybe

interface ITournamentDao {

    val activeTournaments: Maybe<List<Tournament>>
    fun oneHistoryTournamentRow(): Maybe<Boolean>
}
