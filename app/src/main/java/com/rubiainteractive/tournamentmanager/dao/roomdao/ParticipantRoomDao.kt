package com.rubiainteractive.tournamentmanager.dao.roomdao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query

import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant

import io.reactivex.Maybe

@Dao
internal interface ParticipantRoomDao {

    @get:Query("SELECT * FROM Participant")
    val allParticipants: Maybe<List<Participant>>
}
