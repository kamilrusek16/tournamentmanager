package com.rubiainteractive.tournamentmanager.dao.roomdao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentCupRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentLeagueRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper

@Dao
abstract class TournamentConfigRoomDao {

    @Insert
    internal abstract fun insertTournamentRulesConfig(tournamentRulesConfig: TournamentRulesConfig)

    @Insert
    internal abstract fun insertTournamentLeagueRulesConfig(tournamentLeagueRulesConfig: TournamentLeagueRulesConfig)

    @Insert
    internal abstract fun insertTournamentCupRulesConfig(tournamentCupRulesConfig: TournamentCupRulesConfig)

    @Query("SELECT * FROM TournamentRulesConfig WHERE type = :typeId LIMIT 5")
    @Transaction
    abstract fun getTournamentConfig(typeId: Int): List<TournamentRulesConfigWrapper>

    @Transaction
    open fun storeTournamentLeagueRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper) {

        tournamentRulesConfigWrapper.tournamentLeagueRulesConfig.apply {

            if(this?.size == 1) {

                insertTournamentRulesConfig(tournamentRulesConfigWrapper.tournamentRulesConfig!!)
                get(0).parentTournamentConfigId = tournamentRulesConfigWrapper.tournamentRulesConfig!!.id
                insertTournamentLeagueRulesConfig(get(0))
            }
        }
    }

    @Transaction
    open fun storeTournamentCupRulesConfig(tournamentRulesConfigWrapper: TournamentRulesConfigWrapper) {

        tournamentRulesConfigWrapper.tournamentCupRulesConfig.apply {

            if(this?.size == 1) {

                insertTournamentRulesConfig(tournamentRulesConfigWrapper.tournamentRulesConfig!!)
                get(0).parentTournamentConfigId = tournamentRulesConfigWrapper.tournamentRulesConfig!!.id
                insertTournamentCupRulesConfig(get(0))
            }
        }
    }
}
