package com.rubiainteractive.tournamentmanager.dao.roomdao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query

import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament

import io.reactivex.Maybe

@Dao
interface TournamentRoomDao {

    @get:Query("SELECT * FROM Tournament WHERE IsEnded = 0")
    val activeTournaments: Maybe<List<Tournament>>

    @Query("SELECT * FROM Tournament WHERE IsEnded = 1 LIMIT 1")
    fun oneHistoryTournamentRow(): Maybe<List<Tournament>>
}
