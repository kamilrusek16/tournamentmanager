package com.rubiainteractive.tournamentmanager.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.rubiainteractive.tournamentmanager.dao.roomdao.ParticipantRoomDao
import com.rubiainteractive.tournamentmanager.dao.roomdao.TournamentConfigRoomDao
import com.rubiainteractive.tournamentmanager.dao.roomdao.TournamentRoomDao
import com.rubiainteractive.tournamentmanager.entity.dbentities.*


@Database(entities = [Country::class, Game::class, GamePlayer::class, GameTeam::class, Group::class, Participant::class, Player::class, Team::class, Tournament::class, TournamentPlayer::class, TournamentTeam::class, TournamentRulesConfig::class, TournamentLeagueRulesConfig::class, TournamentCupRulesConfig::class], version = 2)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract val tournamentRoomDao: TournamentRoomDao
    internal abstract val participantRoomDao: ParticipantRoomDao
    abstract val tournamentConfigRoomDao: TournamentConfigRoomDao
}
