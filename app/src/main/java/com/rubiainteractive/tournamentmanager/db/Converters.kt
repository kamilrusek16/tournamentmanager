package com.rubiainteractive.tournamentmanager.db

import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

object Converters {

    @JvmStatic
    @android.arch.persistence.room.TypeConverter
    fun toTournamentType(value: Int): TournamentType {
        return TournamentType.getTournamentTypeByValue(value)
    }

    @JvmStatic
    @android.arch.persistence.room.TypeConverter
    fun toInt(tournamentType: TournamentType): Int {
        return tournamentType.value
    }

    @JvmStatic
    @android.arch.persistence.room.TypeConverter
    fun toHigherPlacePriority(value: Int): HigherPlacePriority {
        return HigherPlacePriority.getHigherPlacePriorityByValue(value)
    }

    @JvmStatic
    @android.arch.persistence.room.TypeConverter
    fun toInt(higherPlacePriority: HigherPlacePriority): Int {
        return higherPlacePriority.value
    }
}