package com.rubiainteractive.tournamentmanager.db

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

import com.rubiainteractive.tournamentmanager.db.init.DataInitializer

object DatabaseHelper {

    private const val DB_NAME = "app.db"

    private var appDatabase: AppDatabase? = null

    fun getDatabase(context: Context, dataInitializerList: List<DataInitializer>): AppDatabase {

        if (appDatabase == null) {

            appDatabase = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            for (dataInitializer in dataInitializerList) {
                                dataInitializer.init(db)
                            }
                        }
                    })
                    .fallbackToDestructiveMigration()
                    //.addMigrations(ScriptsV_1_0.get_1_0_migrations())
                    .build()
        }

        return appDatabase as AppDatabase
    }

    fun destroy() {
        appDatabase = null
    }
}
