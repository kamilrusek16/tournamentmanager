package com.rubiainteractive.tournamentmanager.db.init

import android.arch.persistence.db.SupportSQLiteDatabase

interface DataInitializer {
    fun init(db: SupportSQLiteDatabase)
}
