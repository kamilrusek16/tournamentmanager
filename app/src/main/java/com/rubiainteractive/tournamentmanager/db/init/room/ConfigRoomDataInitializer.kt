package com.rubiainteractive.tournamentmanager.db.init.room

import android.arch.persistence.db.SupportSQLiteDatabase
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase

import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.db.init.DataInitializer
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentCupRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentLeagueRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

class ConfigRoomDataInitializer(private val context: Context) : DataInitializer {


    override fun init(db: SupportSQLiteDatabase) {

        storePredefinedTournamentLeagueRulesConfig(db)
        storePredefinedTournamentCupRulesConfig(db)
    }

    private fun storePredefinedTournamentRulesConfig(db: SupportSQLiteDatabase, type: TournamentType): Long {

        val values = ContentValues()

        values.apply {

            put("name", context.resources.getString(R.string.predefined_rules))
            put("description", context.resources.getString(R.string.predefined_rules_desc))
            put("type", type.value)
        }

        return db.insert(TournamentRulesConfig::class.java.simpleName, SQLiteDatabase.CONFLICT_REPLACE, values)
    }

    private fun storePredefinedTournamentLeagueRulesConfig(db: SupportSQLiteDatabase) {

        db.beginTransaction()

        val values = ContentValues()

        values.apply {

            put("HigherPlacePriority", HigherPlacePriority.SmallTable.value)
            put("PointsForWin", 3)
            put("PointsForDraw", 1)
            put("PointsForLoss", 0)
            put("NumberOfGames", 2)
            put("ParentTournamentConfigId", storePredefinedTournamentRulesConfig(db, TournamentType.League))
        }


        db.insert(TournamentLeagueRulesConfig::class.java.simpleName, SQLiteDatabase.CONFLICT_REPLACE, values)

        db.setTransactionSuccessful()
        db.endTransaction()
    }

    private fun storePredefinedTournamentCupRulesConfig(db: SupportSQLiteDatabase) {

        db.beginTransaction()

        val values = ContentValues()

        values.apply {

            put("NumberOfRegularGames", 1)
            put("NumberOfFinalGames", 1)
            put("ParentTournamentConfigId", storePredefinedTournamentRulesConfig(db, TournamentType.Cup))
        }

        db.insert(TournamentCupRulesConfig::class.java.simpleName, SQLiteDatabase.CONFLICT_REPLACE, values)

        db.setTransactionSuccessful()
        db.endTransaction()
    }
}
