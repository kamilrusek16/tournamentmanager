package com.rubiainteractive.tournamentmanager.di.components;

import com.rubiainteractive.tournamentmanager.BaseApplication;
import com.rubiainteractive.tournamentmanager.di.modules.system.ActivityBindingModule;
import com.rubiainteractive.tournamentmanager.di.modules.system.ApplicationModule;
import com.rubiainteractive.tournamentmanager.di.modules.system.ContextModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApplicationModule.class,
        ActivityBindingModule.class
})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        Builder setContextModule(ContextModule contextModule);
        ApplicationComponent build();
    }

    void inject(BaseApplication baseApplication);
}
