package com.rubiainteractive.tournamentmanager.di.modules.business;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;

import com.rubiainteractive.apps.commonutils.utils.permissions.PermissionsHelper;
import com.rubiainteractive.tournamentmanager.R;
import com.rubiainteractive.tournamentmanager.dao.TournamentConfigDao;
import com.rubiainteractive.tournamentmanager.dao.TournamentDao;
import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentConfigDao;
import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao;
import com.rubiainteractive.tournamentmanager.di.scopes.ActivityScope;
import com.rubiainteractive.tournamentmanager.di.scopes.FragmentScope;
import com.rubiainteractive.tournamentmanager.entity.TournamentTypeWrapper;
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType;
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper;
import com.rubiainteractive.tournamentmanager.views.activities.NewTournamentActivity;
import com.rubiainteractive.tournamentmanager.views.adapters.CreateTournamentPagerAdapter;
import com.rubiainteractive.tournamentmanager.views.adapters.ParticipantsAdapter;
import com.rubiainteractive.tournamentmanager.views.fragments.CreateGeneralFragment;
import com.rubiainteractive.tournamentmanager.views.fragments.CreateParticipantsFragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class NewTournamentModule {

    @ActivityScope
    @Binds
    abstract ITournamentDao provideTournamentDao(TournamentDao tournamentDao);

    @ActivityScope
    @Binds
    abstract ITournamentConfigDao provideTournamentConfigDao(TournamentConfigDao configDao);

    @ActivityScope
    @Provides
    static CreateTournamentPagerAdapter provideCreateTournamentPagerAdapter(NewTournamentActivity newTournamentActivity) {
        return new CreateTournamentPagerAdapter(newTournamentActivity.getSupportFragmentManager());
    }

    @FragmentScope
    @Provides
    @Named("CreateGeneralFragmentContext")
    static Context provideCreateGeneralFragmentContext(CreateGeneralFragment fragment) {
        return fragment.getContext();
    }

    @FragmentScope
    @Provides
    static Integer[] provideNumberOfTeamsArray() {
        return new Integer[]{2, 4, 8, 16};
    }

    @FragmentScope
    @Provides
    static ArrayAdapter<Integer> provideNumberOfTeamsAdapter(@Named("CreateGeneralFragmentContext") Context context,
                                                             Integer[] numberOfTeamsArray) {

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, numberOfTeamsArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }

    @FragmentScope
    @Provides
    static TournamentTypeWrapper[] provideModeArray(@Named("CreateGeneralFragmentContext") Context context) {
        return new TournamentTypeWrapper[]{
                new TournamentTypeWrapper(TournamentType.League, context.getResources().getString(R.string.league)),
                new TournamentTypeWrapper(TournamentType.Cup, context.getResources().getString(R.string.cup))
        };
    }

    @FragmentScope
    @Provides
    static ArrayAdapter<TournamentTypeWrapper> provideModeAdapter(@Named("CreateGeneralFragmentContext") Context context,
                                                                  TournamentTypeWrapper[] modesArray) {

        ArrayAdapter<TournamentTypeWrapper> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, modesArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }

    @FragmentScope
    @Provides
    static ArrayAdapter<TournamentRulesConfigWrapper> provideRulesAdapter(@Named("CreateGeneralFragmentContext") Context context) {

        ArrayAdapter<TournamentRulesConfigWrapper> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }

    @FragmentScope
    @Provides
    static PermissionsHelper providePermissionsHelper(CreateGeneralFragment fragment) {
        return new PermissionsHelper(fragment.getActivity());
    }

    @FragmentScope
    @Provides
    @Named("CreateParticipantsFragmentContext")
    static Context provideCreateParticipantsFragmentContext(CreateParticipantsFragment fragment) {
        return fragment.getContext();
    }

    @FragmentScope
    @Provides
    static RecyclerView.LayoutManager provideParticipantsRecyclerView(@Named("CreateParticipantsFragmentContext") Context context) {
        return new LinearLayoutManager(context);
    }

    @FragmentScope
    @Provides
    static ParticipantsAdapter provideParticipantsAdapter(CreateParticipantsFragment fragment) {

        return new ParticipantsAdapter(
                ((NewTournamentActivity) fragment.getActivity())
                        .getNewTournamentViewModel()
                        .getTournamentAllParticipants()
                        .getParticipants()
        );
    }
}
