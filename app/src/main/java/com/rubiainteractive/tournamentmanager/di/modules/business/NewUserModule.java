package com.rubiainteractive.tournamentmanager.di.modules.business;

import com.rubiainteractive.tournamentmanager.dao.ParticipantDao;
import com.rubiainteractive.tournamentmanager.dao.contract.IParticipantDao;
import com.rubiainteractive.tournamentmanager.db.AppDatabase;
import com.rubiainteractive.tournamentmanager.di.scopes.ActivityScope;
import com.rubiainteractive.tournamentmanager.di.scopes.FragmentScope;
import com.rubiainteractive.tournamentmanager.viewmodels.NewUserViewModel;
import com.rubiainteractive.tournamentmanager.views.activities.NewUserActivity;
import com.rubiainteractive.tournamentmanager.views.fragments.NewUserFragment;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class NewUserModule {

    @FragmentScope
    @Provides
    @Named("aaa")
    NewUserActivity provideNewUserActivity(NewUserFragment newUserFragment) {
        return (NewUserActivity) newUserFragment.getActivity();
    }

    @ActivityScope
    @Provides
    IParticipantDao provideParticipantDao(AppDatabase appDatabase) {
        return new ParticipantDao(appDatabase);
    }

    @FragmentScope
    @Provides
    NewUserViewModel provideNewUserViewModel(@Named("aaa") NewUserActivity newUserActivity) {
        return newUserActivity.getNewUserViewModel();
    }
}
