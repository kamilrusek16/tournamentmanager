package com.rubiainteractive.tournamentmanager.di.modules.business;

import android.content.Context;

import com.rubiainteractive.apps.commonutils.utils.rx.CompositeDisposableUtil;
import com.rubiainteractive.apps.utils.viewsentities.DescWithIconPanelEntity;
import com.rubiainteractive.apps.utils.viewsentities.InfoWithIconPanelEntity;
import com.rubiainteractive.tournamentmanager.R;
import com.rubiainteractive.tournamentmanager.dao.TournamentConfigDao;
import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentConfigDao;
import com.rubiainteractive.tournamentmanager.di.scopes.ActivityScope;
import com.rubiainteractive.tournamentmanager.domain.interactors.config.GetTournamentRulesUseCase;
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority;
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType;
import com.rubiainteractive.tournamentmanager.entity.viewentities.BigEditTextPanelEntityRx;
import com.rubiainteractive.tournamentmanager.entity.viewentities.CommonTournamentTypeConfigEntityRx;
import com.rubiainteractive.tournamentmanager.entity.viewentities.ConfigLeaguePointsEntityRx;
import com.rubiainteractive.tournamentmanager.entity.viewentities.CupConfigSummaryEntity;
import com.rubiainteractive.tournamentmanager.entity.viewentities.DescWithIconPanelEntityRx;
import com.rubiainteractive.tournamentmanager.entity.viewentities.LeagueConfigSummaryEntity;
import com.rubiainteractive.tournamentmanager.entity.viewentities.TypeCreatorEntity;
import com.rubiainteractive.tournamentmanager.viewmodels.TournamentTypeCreatorViewModel;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public abstract class TournamentConfigModule {

    @Binds
    abstract ITournamentConfigDao provideTournamentConfigDao(TournamentConfigDao tournamentConfigDao);

    @Provides
    @ActivityScope
    static CompositeDisposable provideCompositeDisposableCollection() {
        return new CompositeDisposable();
    }

    @Provides
    @ActivityScope
    static TypeCreatorEntity provideTypeCreatorEntity() {
        return new TypeCreatorEntity();
    }

    @Provides
    @ActivityScope
    static InfoWithIconPanelEntity provideInfoWithIconPanelEntity(Context context) {

        InfoWithIconPanelEntity entity = new InfoWithIconPanelEntity();
        entity.setInfo(context.getResources().getString(R.string.rules_creator_welcome_info));
        entity.setImageResourceId(R.drawable.ic_edit_120dp);

        return entity;
    }

    @Provides
    @ActivityScope
    static CommonTournamentTypeConfigEntityRx provideCommonTournamentTypeConfigEntity() {
        return new CommonTournamentTypeConfigEntityRx();
    }

    @Provides
    @ActivityScope
    @Named("TypeChooser")
    static DescWithIconPanelEntity provideDescWithIconPanelEntity(Context context,
                                                                  TypeCreatorEntity typeCreatorEntity) {

        DescWithIconPanelEntity descWithIconPanelEntity = new DescWithIconPanelEntity();

        descWithIconPanelEntity.setOption1(context.getResources().getString(R.string.league));
        descWithIconPanelEntity.setOption2(context.getResources().getString(R.string.cup));
        descWithIconPanelEntity.setTitle(context.getResources().getString(R.string.choose_type));
        descWithIconPanelEntity.setSummary(context.getResources().getString(R.string.choosen));
        descWithIconPanelEntity.setSelectedImage1Id(R.drawable.ic_league_120dp);
        descWithIconPanelEntity.setSelectedImage2Id(R.drawable.cup);
        descWithIconPanelEntity.setUnselectedImage1Id(R.drawable.ic_league_120dp);
        descWithIconPanelEntity.setUnselectedImage2Id(R.drawable.cup);
        descWithIconPanelEntity.setFinalSelectedImage1Id(R.drawable.ic_league_120dp);
        descWithIconPanelEntity.setFinalSelectedImage2Id(R.drawable.cup);

        descWithIconPanelEntity.addOptionOneChooseConsumer(aBoolean -> {

            if(aBoolean)
                typeCreatorEntity.setTournamentType(TournamentType.League.getValue());
            else
                typeCreatorEntity.setTournamentType(-1);
        });

        descWithIconPanelEntity.addOptionTwoChooseConsumer(aBoolean -> {

            if(aBoolean)
                typeCreatorEntity.setTournamentType(TournamentType.Cup.getValue());
            else
                typeCreatorEntity.setTournamentType(-1);
        });

        return descWithIconPanelEntity;
    }

    @Provides
    @ActivityScope
    @Named("BigEditTextPanelEntityLeague3")
    static BigEditTextPanelEntityRx provideBigEditTextPanelEntityLeague3(Context context) {

        BigEditTextPanelEntityRx bigEditTextPanelEntity = new BigEditTextPanelEntityRx();

        bigEditTextPanelEntity.setTitle(context.getResources().getString(R.string.number_of_games));
        bigEditTextPanelEntity.setEditTextValueInt(2);
        bigEditTextPanelEntity.setMaxErrorMessage(
                context.getString(R.string.max_games_error, bigEditTextPanelEntity.getMaxValue())
        );
        bigEditTextPanelEntity.setMinErrorMessage(
                context.getString(R.string.min_games_error, bigEditTextPanelEntity.getMinValue())
        );

        return bigEditTextPanelEntity;
    }

    @Provides
    @ActivityScope
    @Named("BigEditTextPanelEntityCup3")
    static BigEditTextPanelEntityRx provideBigEditTextPanelEntityCup3(Context context) {

        BigEditTextPanelEntityRx bigEditTextPanelEntity = new BigEditTextPanelEntityRx();

        bigEditTextPanelEntity.setTitle(context.getResources().getString(R.string.number_of_games_in_regular_stage));
        bigEditTextPanelEntity.setEditTextValueInt(1);
        bigEditTextPanelEntity.setMaxErrorMessage(
                context.getString(R.string.max_games_error, bigEditTextPanelEntity.getMaxValue())
        );
        bigEditTextPanelEntity.setMinErrorMessage(
                context.getString(R.string.min_games_error, bigEditTextPanelEntity.getMinValue())
        );

        return bigEditTextPanelEntity;
    }

    @Provides
    @ActivityScope
    @Named("BigEditTextPanelEntityCup4")
    static BigEditTextPanelEntityRx provideBigEditTextPanelEntityCup4(Context context) {

        BigEditTextPanelEntityRx bigEditTextPanelEntity = new BigEditTextPanelEntityRx();

        bigEditTextPanelEntity.setTitle(context.getResources().getString(R.string.number_of_games_in_final));
        bigEditTextPanelEntity.setEditTextValueInt(2);
        bigEditTextPanelEntity.setMaxErrorMessage(
                context.getString(R.string.max_games_error, bigEditTextPanelEntity.getMaxValue())
        );
        bigEditTextPanelEntity.setMinErrorMessage(
                context.getString(R.string.min_games_error, bigEditTextPanelEntity.getMinValue())
        );

        return bigEditTextPanelEntity;
    }

    @Provides
    @ActivityScope
    static CupConfigSummaryEntity provideCupConfigSummaryEntity(@Named("BigEditTextPanelEntityCup3") BigEditTextPanelEntityRx bigEditTextPanelEntityCup3,
                                                                @Named("BigEditTextPanelEntityCup4") BigEditTextPanelEntityRx bigEditTextPanelEntityCup4,
                                                                CommonTournamentTypeConfigEntityRx commonTournamentTypeConfigEntity,
                                                                CompositeDisposable disposables,
                                                                Context context) {

        CupConfigSummaryEntity cupConfigSummaryEntity = new CupConfigSummaryEntity(context);

        cupConfigSummaryEntity.setNumberOfGames(bigEditTextPanelEntityCup3.getEditTextValueInt());
        cupConfigSummaryEntity.setNumberOfFinalGames(bigEditTextPanelEntityCup4.getEditTextValueInt());

        CompositeDisposableUtil.add(
                disposables,
                bigEditTextPanelEntityCup3
                        .getModelChanges()
                        .subscribe(event -> cupConfigSummaryEntity.setNumberOfGames(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                bigEditTextPanelEntityCup4
                        .getModelChanges()
                        .subscribe(event -> cupConfigSummaryEntity.setNumberOfFinalGames(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                commonTournamentTypeConfigEntity
                        .getModelChanges().subscribe(event -> cupConfigSummaryEntity.setName(event.getValue()))
        );

        return cupConfigSummaryEntity;
    }

    @Provides
    @ActivityScope
    static LeagueConfigSummaryEntity provideLeagueConfigSummaryEntity(
            Context context,
            @Named("BigEditTextPanelEntityLeague3") BigEditTextPanelEntityRx bigEditTextPanelEntityLeague3,
            ConfigLeaguePointsEntityRx configLeaguePointsEntityRx,
            @Named("LeaguePriority") DescWithIconPanelEntityRx leaguePriorityEntityRx,
            CommonTournamentTypeConfigEntityRx commonTournamentTypeConfigEntity,
            CompositeDisposable disposables) {

        LeagueConfigSummaryEntity leagueConfigSummaryEntity = new LeagueConfigSummaryEntity(context);

        leagueConfigSummaryEntity.setNumberOfGames(bigEditTextPanelEntityLeague3.getEditTextValueInt());
        leagueConfigSummaryEntity.setPointsForWin(configLeaguePointsEntityRx.getPointsForWin());
        leagueConfigSummaryEntity.setPointsForDraw(configLeaguePointsEntityRx.getPointsForDraw());
        leagueConfigSummaryEntity.setPointsForLost(configLeaguePointsEntityRx.getPointsForLoss());

        if(leaguePriorityEntityRx.isOptionOneSelected())

        leagueConfigSummaryEntity.setPriority(
                leaguePriorityEntityRx.isOptionOneSelected()
                        ? HigherPlacePriority.GoalsDifference
                        : HigherPlacePriority.SmallTable
        );

        CompositeDisposableUtil.add(
                disposables,
                bigEditTextPanelEntityLeague3
                        .getModelChanges()
                        .subscribe(event -> leagueConfigSummaryEntity.setNumberOfGames(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                configLeaguePointsEntityRx
                        .getPointsForWinChanges()
                        .subscribe(event -> leagueConfigSummaryEntity.setPointsForWin(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                configLeaguePointsEntityRx
                        .getPointsForDrawChanges()
                        .subscribe(event -> leagueConfigSummaryEntity.setPointsForDraw(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                configLeaguePointsEntityRx
                        .getPointsForLostChanges()
                        .subscribe(event -> leagueConfigSummaryEntity.setPointsForLost(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                leaguePriorityEntityRx
                        .getModelChanges()
                        .subscribe(event -> leagueConfigSummaryEntity.setPriority(event.getValue()))
        );

        CompositeDisposableUtil.add(
                disposables,
                commonTournamentTypeConfigEntity
                        .getModelChanges().subscribe(event -> leagueConfigSummaryEntity.setName(event.getValue()))
        );

        return leagueConfigSummaryEntity;
    }

    @Provides
    @ActivityScope
    static ConfigLeaguePointsEntityRx provideConfigLeaguePointsEntity() {

        ConfigLeaguePointsEntityRx entity = new ConfigLeaguePointsEntityRx();
        entity.setPointsForWin(3);
        entity.setPointsForDraw(1);
        entity.setPointsForLoss(0);

        return entity;
    }

    @Provides
    @ActivityScope
    @Named("LeaguePriority")
    static DescWithIconPanelEntityRx provideLeaguePriorityEntity(Context context) {

        DescWithIconPanelEntityRx leaguePriorityEntity = new DescWithIconPanelEntityRx();
        leaguePriorityEntity.setTitle(context.getResources().getString(R.string.priority));

        leaguePriorityEntity.setSelectedImage1Id(R.drawable.goals);
        leaguePriorityEntity.setUnselectedImage1Id(R.drawable.goals);
        leaguePriorityEntity.setFinalSelectedImage1Id(R.drawable.goals);
        leaguePriorityEntity.setOption1(context.getResources().getString(R.string.goals));

        leaguePriorityEntity.setSelectedImage2Id(R.drawable.ic_small_table);
        leaguePriorityEntity.setUnselectedImage2Id(R.drawable.ic_small_table);
        leaguePriorityEntity.setFinalSelectedImage2Id(R.drawable.ic_small_table);
        leaguePriorityEntity.setOption2(context.getResources().getString(R.string.small_table));

        leaguePriorityEntity.setOptionOneSelected(true);

        leaguePriorityEntity.setSummary(context.getResources().getString(R.string.choosen));

        return leaguePriorityEntity;
    }

    @Binds
    @ActivityScope
    abstract GetTournamentRulesUseCase provideGetTournamentRulesUseCase(GetTournamentRulesUseCase getTournamentRulesUseCase);

    @Provides
    @ActivityScope
    static TournamentTypeCreatorViewModel.Entities provideViewModelEntities(
            CompositeDisposable disposables,
            TypeCreatorEntity typeCreatorEntity,
            CommonTournamentTypeConfigEntityRx commonTournamentTypeConfigEntity,
            InfoWithIconPanelEntity infoWithIconPanelEntity,
            @Named("TypeChooser") DescWithIconPanelEntity descWithIconPanelEntity,
            @Named("BigEditTextPanelEntityLeague3") BigEditTextPanelEntityRx bigEditTextPanelEntityLeague3,
            @Named("BigEditTextPanelEntityCup3") BigEditTextPanelEntityRx bigEditTextPanelEntityCup3,
            @Named("BigEditTextPanelEntityCup4") BigEditTextPanelEntityRx bigEditTextPanelEntityCup4,
            CupConfigSummaryEntity cupConfigSummaryEntity,
            ConfigLeaguePointsEntityRx configLeaguePointsEntity,
            @Named("LeaguePriority") DescWithIconPanelEntityRx leaguePriorityEntity,
            LeagueConfigSummaryEntity leagueConfigSummaryEntity) {

        return new TournamentTypeCreatorViewModel.Entities()
                .setDisposables(disposables)
                .setTypeCreatorEntity(typeCreatorEntity)
                .setCommonTournamentTypeConfigEntity(commonTournamentTypeConfigEntity)
                .setInfoWithIconPanelEntity(infoWithIconPanelEntity)
                .setDescWithIconPanelEntity(descWithIconPanelEntity)
                .setBigEditTextPanelEntityCup3(bigEditTextPanelEntityCup3)
                .setBigEditTextPanelEntityCup4(bigEditTextPanelEntityCup4)
                .setBigEditTextPanelEntityLeague3(bigEditTextPanelEntityLeague3)
                .setCupConfigSummaryEntity(cupConfigSummaryEntity)
                .setConfigLeaguePointsEntity(configLeaguePointsEntity)
                .setLeaguePriorityEntity(leaguePriorityEntity)
                .setLeagueConfigSummaryEntity(leagueConfigSummaryEntity);
    }
}
