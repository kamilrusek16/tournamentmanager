package com.rubiainteractive.tournamentmanager.di.modules.business;

import com.rubiainteractive.tournamentmanager.dao.TournamentDao;
import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao;
import com.rubiainteractive.tournamentmanager.domain.interactors.launch.GetActiveTournamentsUseCase;
import com.rubiainteractive.tournamentmanager.domain.interactors.launch.IsAnyHistoricalTournamentUseCase;
import com.rubiainteractive.tournamentmanager.viewmodels.factories.LaunchViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class TournamentLoadingModule {

    @Binds
    abstract ITournamentDao provideTournamentDao(TournamentDao tournamentDao);

    @Provides
    static LaunchViewModelFactory provideLaunchViewModelFactory(GetActiveTournamentsUseCase getActiveTournamentsUseCase,
                                                         IsAnyHistoricalTournamentUseCase isAnyHistoricalTournamentUseCase) {

        return new LaunchViewModelFactory(getActiveTournamentsUseCase, isAnyHistoricalTournamentUseCase);
    }
}
