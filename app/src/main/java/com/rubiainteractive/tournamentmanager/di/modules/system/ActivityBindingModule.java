package com.rubiainteractive.tournamentmanager.di.modules.system;

import com.rubiainteractive.tournamentmanager.di.modules.business.NewTournamentModule;
import com.rubiainteractive.tournamentmanager.di.modules.business.NewUserModule;
import com.rubiainteractive.tournamentmanager.di.modules.business.TournamentConfigModule;
import com.rubiainteractive.tournamentmanager.di.modules.business.TournamentLoadingModule;
import com.rubiainteractive.tournamentmanager.di.scopes.ActivityScope;
import com.rubiainteractive.tournamentmanager.views.activities.MainActivity;
import com.rubiainteractive.tournamentmanager.views.activities.NewTournamentActivity;
import com.rubiainteractive.tournamentmanager.views.activities.NewUserActivity;
import com.rubiainteractive.tournamentmanager.views.activities.TournamentTypeCreatorActivity;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ActivityKey;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = TournamentLoadingModule.class)
    abstract MainActivity bindMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = { NewTournamentModule.class, FragmentBindingModule.class })
    abstract NewTournamentActivity bindNewTournamentActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = { NewUserModule.class, FragmentBindingModule.class })
    abstract NewUserActivity bindNewUserActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = { TournamentConfigModule.class })
    abstract TournamentTypeCreatorActivity bindTournamentTypeCreatorActivity();

}
