package com.rubiainteractive.tournamentmanager.di.modules.system;

import android.content.Context;

import com.rubiainteractive.tournamentmanager.db.AppDatabase;
import com.rubiainteractive.tournamentmanager.db.DatabaseHelper;
import com.rubiainteractive.tournamentmanager.db.init.DataInitializer;
import com.rubiainteractive.tournamentmanager.db.init.room.ConfigRoomDataInitializer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class ApplicationModule {

    @Provides
    List<DataInitializer> provideDataInitializersList(Context context) {

        List<DataInitializer> result = new ArrayList<>();

        result.add(new ConfigRoomDataInitializer(context));

        return result;
    }

    @Singleton
    @Provides
    AppDatabase provideAppDatabase(Context context, List<DataInitializer> dataInitializerList){
        return DatabaseHelper.INSTANCE.getDatabase(context, dataInitializerList);
    }
}
