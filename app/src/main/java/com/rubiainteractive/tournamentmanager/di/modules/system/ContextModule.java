package com.rubiainteractive.tournamentmanager.di.modules.system;

import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private Context context;

    public ContextModule(@NonNull Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return this.context;
    }
}
