package com.rubiainteractive.tournamentmanager.di.modules.system;

import com.rubiainteractive.tournamentmanager.di.modules.business.NewTournamentModule;
import com.rubiainteractive.tournamentmanager.di.modules.business.NewUserModule;
import com.rubiainteractive.tournamentmanager.di.scopes.ActivityScope;
import com.rubiainteractive.tournamentmanager.di.scopes.FragmentScope;
import com.rubiainteractive.tournamentmanager.views.fragments.CreateGeneralFragment;
import com.rubiainteractive.tournamentmanager.views.fragments.CreateParticipantsFragment;
import com.rubiainteractive.tournamentmanager.views.fragments.NewUserFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBindingModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = NewTournamentModule.class)
    abstract CreateGeneralFragment bindNewTournamentGeneralFragment();

    @FragmentScope
    @ContributesAndroidInjector(modules = NewTournamentModule.class)
    abstract CreateParticipantsFragment bindNewTournamentParticipantsFragment();

    @FragmentScope
    @ContributesAndroidInjector(modules = NewUserModule.class)
    abstract NewUserFragment bindNewUserFragment();
}
