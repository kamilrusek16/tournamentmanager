package com.rubiainteractive.tournamentmanager.domain.interactors

interface Interactor<T> {

    fun execute(): T
}
