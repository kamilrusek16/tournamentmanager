package com.rubiainteractive.tournamentmanager.domain.interactors

interface Interactor1<T, V> {

    fun execute(param: V): T
}
