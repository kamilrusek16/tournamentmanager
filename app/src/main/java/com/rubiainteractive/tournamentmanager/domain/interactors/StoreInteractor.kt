package com.rubiainteractive.tournamentmanager.domain.interactors

interface StoreInteractor<T> {
    fun execute(arg: T)
}
