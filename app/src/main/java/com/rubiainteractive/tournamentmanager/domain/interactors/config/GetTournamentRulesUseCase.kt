package com.rubiainteractive.tournamentmanager.domain.interactors.config

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentConfigDao
import com.rubiainteractive.tournamentmanager.domain.interactors.Interactor1
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper

import javax.inject.Inject

class GetTournamentRulesUseCase @Inject
internal constructor() : Interactor1<List<TournamentRulesConfigWrapper>, Int?> {

    @Inject
    lateinit var tournamentConfigDao: ITournamentConfigDao

    override fun execute(param: Int?): List<TournamentRulesConfigWrapper> {
        return tournamentConfigDao.getTournamentConfig(param!!)
    }
}
