package com.rubiainteractive.tournamentmanager.domain.interactors.config

import com.rubiainteractive.apps.commonutils.utils.db.DisposableUseCase
import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentConfigDao
import com.rubiainteractive.tournamentmanager.domain.interactors.StoreInteractor
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper

import javax.inject.Inject

import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class StoreTournamentRulesUseCase @Inject
constructor() : DisposableUseCase(), StoreInteractor<TournamentRulesConfigWrapper> {

    @Inject
    lateinit var tournamentConfigDao: ITournamentConfigDao

    override fun execute(arg: TournamentRulesConfigWrapper) {

        add(Completable.fromAction { tournamentConfigDao.storeTournamentCupRulesConfig(arg) }
                .subscribeOn(Schedulers.io())
                .subscribe())
    }
}
