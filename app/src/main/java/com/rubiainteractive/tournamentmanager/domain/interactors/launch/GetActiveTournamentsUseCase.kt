package com.rubiainteractive.tournamentmanager.domain.interactors.launch

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao
import com.rubiainteractive.tournamentmanager.domain.interactors.Interactor
import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament
import io.reactivex.Maybe
import javax.inject.Inject

class GetActiveTournamentsUseCase @Inject
internal constructor() : Interactor<Maybe<List<Tournament>>> {

    @Inject
    lateinit var tournamentDao: ITournamentDao

    override fun execute(): Maybe<List<Tournament>> {
        return tournamentDao.activeTournaments
    }
}
