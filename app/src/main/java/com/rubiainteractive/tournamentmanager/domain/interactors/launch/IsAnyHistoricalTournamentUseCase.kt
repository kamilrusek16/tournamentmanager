package com.rubiainteractive.tournamentmanager.domain.interactors.launch

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao
import com.rubiainteractive.tournamentmanager.domain.interactors.Interactor

import javax.inject.Inject

import io.reactivex.Maybe

class IsAnyHistoricalTournamentUseCase @Inject
constructor() : Interactor<Maybe<Boolean>> {

    @Inject
    lateinit var tournamentDao: ITournamentDao

    override fun execute(): Maybe<Boolean> {
        return tournamentDao.oneHistoryTournamentRow()
    }
}
