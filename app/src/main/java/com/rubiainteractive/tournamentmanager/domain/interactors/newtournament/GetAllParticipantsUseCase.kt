package com.rubiainteractive.tournamentmanager.domain.interactors.newtournament

import com.rubiainteractive.tournamentmanager.dao.contract.IParticipantDao
import com.rubiainteractive.tournamentmanager.domain.interactors.Interactor
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant

import javax.inject.Inject

import io.reactivex.Maybe

class GetAllParticipantsUseCase @Inject
internal constructor() : Interactor<Maybe<List<Participant>>> {

    @Inject
    lateinit var participantDao: IParticipantDao

    override fun execute(): Maybe<List<Participant>> {
        return participantDao.allParticipants
    }
}
