package com.rubiainteractive.tournamentmanager.domain.interactors.newtournament

import com.rubiainteractive.tournamentmanager.dao.contract.ITournamentDao
import com.rubiainteractive.tournamentmanager.db.AppDatabase
import com.rubiainteractive.tournamentmanager.domain.interactors.StoreInteractor
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentAllParticipants

import javax.inject.Inject

class StoreTournamentUseCase @Inject
internal constructor() : StoreInteractor<TournamentAllParticipants> {

    @Inject
    lateinit var tournamentDao: ITournamentDao

    override fun execute(arg: TournamentAllParticipants) {

    }
}
