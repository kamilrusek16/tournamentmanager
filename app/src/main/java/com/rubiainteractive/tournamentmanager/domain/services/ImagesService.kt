package com.rubiainteractive.tournamentmanager.domain.services

import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore

import com.rubiainteractive.apps.commonutils.utils.files.FileHandler

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class ImagesService {

    private val imagesDir: String
        get() = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path
                + File.separator + "TournamentManager")

    fun getDispatchTakePictureIntent(fileHandler: FileHandler): Intent {

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        fileHandler.createFile(imagesDir, generateImageName())

        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileHandler.file))

        return i
    }

    private fun generateImageName(): String {

        return SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
                .format(Calendar.getInstance().time) + ".png"
    }
}
