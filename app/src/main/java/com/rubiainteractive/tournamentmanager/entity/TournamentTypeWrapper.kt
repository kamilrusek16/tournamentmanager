package com.rubiainteractive.tournamentmanager.entity

import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

class TournamentTypeWrapper(val tournamentType: TournamentType, private val visibleName: String) {

    override fun toString(): String {
        return visibleName
    }
}
