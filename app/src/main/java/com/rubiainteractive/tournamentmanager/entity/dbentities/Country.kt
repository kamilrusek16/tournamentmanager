package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
data class Country constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var name: String? = null,
        var imageUrl: String? = null
)
