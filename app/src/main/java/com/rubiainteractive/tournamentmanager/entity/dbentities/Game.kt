package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(entity = GameTeam::class, parentColumns = arrayOf("id"), childColumns = arrayOf("team1Id"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = GameTeam::class, parentColumns = arrayOf("id"), childColumns = arrayOf("team2Id"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = Group::class, parentColumns = arrayOf("id"), childColumns = arrayOf("groupId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["team1Id"]), Index(value = ["team2Id"]), Index(value = ["groupId"])])
data class Game constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var round: Int = 0,
        var team1Id: Long = 0,
        var team2Id: Long = 0,
        var groupId: Long = 0
)
