package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(entity = GameTeam::class, parentColumns = arrayOf("id"), childColumns = arrayOf("gameTeamId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = TournamentPlayer::class, parentColumns = arrayOf("id"), childColumns = arrayOf("tournamentPlayerId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["gameTeamId"]), Index(value = ["tournamentPlayerId"])])
data class GamePlayer constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        @Ignore var isPlayed: Boolean = false,
        var isFirstEleven: Boolean = false,
        var goals: Int = 0,
        var assists: Int = 0,
        var yellowCards: Int = 0,
        var redCards: Int = 0,
        var goalsLost: Int = 0,
        var gameTeamId: Long = 0,
        var tournamentPlayerId: Long = 0
)
