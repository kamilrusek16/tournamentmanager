package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey


@Entity(foreignKeys = [ForeignKey(entity = Game::class, parentColumns = arrayOf("id"), childColumns = arrayOf("gameId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = TournamentTeam::class, parentColumns = arrayOf("id"), childColumns = arrayOf("tournamentTeamId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["gameId"]), Index(value = ["tournamentTeamId"])])
data class GameTeam constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var gameId: Long = 0,
        var points: Int = 0,
        var goals: Int = 0,
        var tournamentTeamId: Long = 0
)
