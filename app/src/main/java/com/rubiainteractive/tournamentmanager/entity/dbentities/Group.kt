package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(entity = Tournament::class, parentColumns = arrayOf("id"), childColumns = arrayOf("tournamentId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["tournamentId"])])
data class Group constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var groupOrder: Int = 0,

        /*
        in league case always 1, when cup - cup phase
         */
        var groupLevel: Int = 0,

        /*
        used in cup, indicate next groupOrder in tournament table
         */
        var nextGroupId: Int = 0,
        var isStageEnded: Boolean = false,
        var tournamentId: Long = 0
)
