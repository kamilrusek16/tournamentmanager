package com.rubiainteractive.tournamentmanager.entity.dbentities


import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.databinding.BaseObservable
import android.databinding.Bindable

import com.rubiainteractive.tournamentmanager.BR

import java.io.Serializable

@Entity(foreignKeys = [ForeignKey(entity = Tournament::class, parentColumns = arrayOf("id"), childColumns = arrayOf("tournamentId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["tournamentId"])])
class Participant : BaseObservable(), Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    var tournamentId: Long = 0

    @get:Bindable
    var name: String? = null
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }

    @Ignore
    @get:Bindable
    var isImmediatelySave = true
        set(immediatelySave) {
            field = immediatelySave
            notifyPropertyChanged(BR.immediatelySave)
        }
}
