package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters

import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.PlayerPositionType

@Entity(foreignKeys = [ForeignKey(entity = Country::class, parentColumns = arrayOf("id"), childColumns = arrayOf("countryId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = Team::class, parentColumns = arrayOf("id"), childColumns = arrayOf("teamId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["countryId"]), Index(value = ["teamId"])])
class Player {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    var playerNumber: Int = 0

    var firstName: String? = null

    var lastName: String? = null

    var imageUrl: String? = null
        get() = if (field == null) "players//empty_player_image" else field

    @TypeConverters(PlayerPositionType.TypeConverter::class)
    var playerPositionType: PlayerPositionType? = null

    var countryId: Long = 0

    var teamId: Long = 0

    constructor() {}

    constructor(firstName: String, lastName: String, imageUrl: String, positionType: PlayerPositionType) {

        this.firstName = firstName
        this.lastName = lastName
        this.imageUrl = imageUrl
        this.playerPositionType = positionType
    }
}
