package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.*

@Entity(foreignKeys = [ForeignKey(entity = Country::class, parentColumns = arrayOf("id"), childColumns = arrayOf("countryId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["countryId"])])
data class Team constructor(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var name: String? = null,
    var abbreviation: String? = null,
    var imageUrl: String? = null,
    var countryId: Long = 0
) {
    @Ignore
    constructor(name: String, imageUrl: String) : this() {
        this.name = name
        this.imageUrl = imageUrl
    }
}
