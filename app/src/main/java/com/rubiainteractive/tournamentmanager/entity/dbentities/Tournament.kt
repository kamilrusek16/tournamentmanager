package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.*
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Bitmap
import com.rubiainteractive.tournamentmanager.BR
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

@Entity(foreignKeys = [ForeignKey(entity = TournamentRulesConfig::class, parentColumns = arrayOf("id"), childColumns = arrayOf("rulesConfigId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["rulesConfigId"])])
class Tournament : BaseObservable() {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @get:Bindable
    var tournamentName: String? = null
        set(tournamentName) {
            field = tournamentName
            notifyPropertyChanged(BR.tournamentName)
        }

    var tournamentType: TournamentType? = null

    var rulesConfigId: Long = 0

    @get:Bindable
    var teamsCount: Int = 0
        set(teamsCount) {
            field = teamsCount
            notifyPropertyChanged(BR.teamsCount)
        }

    var isEnded: Boolean = false

    var imageUrl: String? = null

    @Ignore
    var image: Bitmap? = null

    override fun toString(): String {
        return this.tournamentName ?: ""
    }
}
