package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.rubiainteractive.tournamentmanager.entity.dbentities.base.TournamentRulesConfigBase

@Entity(foreignKeys = [ForeignKey(entity = TournamentRulesConfig::class, parentColumns = arrayOf("id"), childColumns = arrayOf("parentTournamentConfigId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["parentTournamentConfigId"])])
data class TournamentCupRulesConfig constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var numberOfRegularGames: Int = 0,
        var numberOfFinalGames: Int = 0
) : TournamentRulesConfigBase()
