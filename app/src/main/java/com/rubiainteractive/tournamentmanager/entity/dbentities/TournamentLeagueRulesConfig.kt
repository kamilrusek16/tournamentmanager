package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.*
import com.rubiainteractive.tournamentmanager.entity.dbentities.base.TournamentRulesConfigBase
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority

@Entity(foreignKeys = [ForeignKey(entity = TournamentRulesConfig::class, parentColumns = arrayOf("id"), childColumns = arrayOf("parentTournamentConfigId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["parentTournamentConfigId"])])
data class TournamentLeagueRulesConfig constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var higherPlacePriority: HigherPlacePriority? = null,
        var pointsForWin: Int = 0,
        var pointsForDraw: Int = 0,
        var pointsForLoss: Int = 0,
        var numberOfGames: Int = 0
) : TournamentRulesConfigBase()
