package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(entity = TournamentTeam::class, parentColumns = arrayOf("id"), childColumns = arrayOf("tournamentTeamId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = Player::class, parentColumns = arrayOf("id"), childColumns = arrayOf("playerId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["tournamentTeamId"]), Index(value = ["playerId"])])
data class TournamentPlayer constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var tournamentTeamId: Long = 0,
        var appearances: Int = 0,
        var goals: Int = 0,
        var assists: Int = 0,
        var yellowCards: Int = 0,
        var redCards: Int = 0,
        var goalsLost: Int = 0,
        var playerId: Long = 0
)
