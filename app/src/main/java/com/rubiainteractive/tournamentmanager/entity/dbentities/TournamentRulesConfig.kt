package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters

import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

@Entity(indices = [Index(value = ["id"])])
data class TournamentRulesConfig constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var type: TournamentType? = null,
        var name: String? = null,
        var description: String? = null) {


    override fun toString(): String {
        return this.name ?: ""
    }
}
