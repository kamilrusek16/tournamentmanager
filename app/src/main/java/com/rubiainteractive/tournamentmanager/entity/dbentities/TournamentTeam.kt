package com.rubiainteractive.tournamentmanager.entity.dbentities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(entity = Group::class, parentColumns = arrayOf("id"), childColumns = arrayOf("groupId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = Participant::class, parentColumns = arrayOf("id"), childColumns = arrayOf("participantId"), onDelete = ForeignKey.CASCADE), ForeignKey(entity = Team::class, parentColumns = arrayOf("id"), childColumns = arrayOf("teamId"), onDelete = ForeignKey.CASCADE)], indices = [Index(value = ["id"]), Index(value = ["groupId"]), Index(value = ["participantId"]), Index(value = ["teamId"])])
data class TournamentTeam constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var gamesPlayed: Int = 0,
        var wins: Int = 0,
        var draws: Int = 0,
        var losses: Int = 0,
        var teamPoints: Int = 0, // one store operation to database, only read, no need to recalculate points every time
        var teamGoalsPlus: Int = 0,
        var teamGoalsMinus: Int = 0,
        var groupId: Long = 0,
        var participantId: Long = 0,
        var teamId: Long = 0
) : Comparable<TournamentTeam> {

    override fun compareTo(other: TournamentTeam): Int { // in next versions separate to rules (ex. FIFA, UEFA...), method can be move to separate service

        return when {
            this.teamPoints > other.teamPoints -> 1
            this.teamPoints < other.teamPoints -> -1
            else -> when {
                this.teamGoalsPlus - this.teamGoalsMinus > other.teamGoalsPlus - other.teamGoalsMinus -> 1
                this.teamGoalsPlus - this.teamGoalsMinus < other.teamGoalsPlus - other.teamGoalsMinus -> -1
                else -> when {
                    this.teamGoalsPlus > other.teamGoalsPlus -> 1
                    this.teamGoalsPlus < other.teamGoalsPlus -> -1
                    else -> 0
                }
            }
        }
    }
}
