package com.rubiainteractive.tournamentmanager.entity.dbentities.enums

enum class HigherPlacePriority constructor(val value: Int) {

    SmallTable(0),
    GoalsDifference(1);

    companion object {

        fun getHigherPlacePriorityByValue(value: Int): HigherPlacePriority {

            when (value) {

                0 -> return SmallTable
                1 -> return GoalsDifference
            }

            return SmallTable // default
        }
    }
}
