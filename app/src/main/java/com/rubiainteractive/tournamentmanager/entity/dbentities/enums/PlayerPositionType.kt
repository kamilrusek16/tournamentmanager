package com.rubiainteractive.tournamentmanager.entity.dbentities.enums

enum class PlayerPositionType constructor(val value: Int) {

    GK(0),
    WB(1),
    CB(2),
    CM(3),
    WM(4),
    CF(5),
    Undefined(6);

    object TypeConverter {

        @android.arch.persistence.room.TypeConverter
        fun toPlayerPositionType(value: Int): PlayerPositionType {
            when (value) {

                0 -> return PlayerPositionType.GK
                1 -> return PlayerPositionType.WB
                2 -> return PlayerPositionType.CB
                3 -> return PlayerPositionType.CM
                4 -> return PlayerPositionType.WM
                5 -> return PlayerPositionType.CF
                else -> return PlayerPositionType.Undefined
            }
        }

        @android.arch.persistence.room.TypeConverter
        fun toInt(playerPositionType: PlayerPositionType): Int {
            return playerPositionType.value
        }
    }
}
