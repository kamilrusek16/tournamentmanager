package com.rubiainteractive.tournamentmanager.entity.dbentities.enums

enum class TournamentType constructor(val value: Int) {

    League(0),
    Cup(1);

    companion object {

        fun getTournamentTypeByValue(value: Int): TournamentType {

            when (value) {

                0 -> return League
                1 -> return Cup
            }

            return League // default
        }
    }
}
