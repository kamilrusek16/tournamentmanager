package com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers

import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import android.databinding.BaseObservable
import android.databinding.Bindable
import com.android.databinding.library.baseAdapters.BR
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant
import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament
import java.util.*

class TournamentAllParticipants : BaseObservable() {

    @Embedded
    private var tournament: Tournament? = null

    @Relation(parentColumn = "id", entityColumn = "tournamentId", entity = Participant::class)
    val participants: MutableLiveData<List<Participant>>

    var tournamentRulesConfigWrapper: TournamentRulesConfigWrapper? = null

    val participantsList: List<Participant>?
        get() = participants.value

    init {
        this.tournament = Tournament()
        this.participants = MutableLiveData()
        this.participants.value = ArrayList()
    }

    @Bindable
    fun getTournament(): Tournament? {
        return tournament
    }

    fun setTournament(tournament: Tournament) {
        this.tournament = tournament
        notifyPropertyChanged(BR.tournament)
    }
}
