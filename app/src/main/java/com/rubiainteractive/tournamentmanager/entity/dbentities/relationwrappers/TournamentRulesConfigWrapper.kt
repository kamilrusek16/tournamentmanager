package com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentCupRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentLeagueRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.base.TournamentRulesConfigBase
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType

class TournamentRulesConfigWrapper {

    @Embedded
    var tournamentRulesConfig: TournamentRulesConfig? = null

    @Relation(parentColumn = "id", entityColumn = "parentTournamentConfigId", entity = TournamentLeagueRulesConfig::class)
    var tournamentLeagueRulesConfig: List<TournamentLeagueRulesConfig>? = null

    @Relation(parentColumn = "id", entityColumn = "parentTournamentConfigId", entity = TournamentCupRulesConfig::class)
    var tournamentCupRulesConfig: List<TournamentCupRulesConfig>? = null

    fun get(): TournamentRulesConfigBase {

        return if (tournamentRulesConfig!!.type === TournamentType.League)
            tournamentLeagueRulesConfig!![0]
        else
            tournamentCupRulesConfig!![0]
    }

    override fun toString(): String {
        return tournamentRulesConfig!!.name!!
    }
}
