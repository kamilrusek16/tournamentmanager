package com.rubiainteractive.tournamentmanager.entity.events

class ChangeTypeEvent(val selectedTournamentType: Int)
