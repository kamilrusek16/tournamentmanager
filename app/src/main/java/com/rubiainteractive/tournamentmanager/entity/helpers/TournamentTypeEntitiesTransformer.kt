package com.rubiainteractive.tournamentmanager.entity.helpers

import android.content.Context
import com.rubiainteractive.apps.commonutils.utils.StringUtils
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentCupRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentLeagueRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.TournamentRulesConfig
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper
import com.rubiainteractive.tournamentmanager.entity.validation.ValidationResult
import com.rubiainteractive.tournamentmanager.viewmodels.TournamentTypeCreatorViewModel

class TournamentTypeEntitiesTransformer {

    fun validateModel(context: Context, entities: TournamentTypeCreatorViewModel.Entities): ValidationResult {

        return if (StringUtils.isNullOrEmpty(entities.commonTournamentTypeConfigEntity?.name)) ValidationResult(false, context.resources.getString(R.string.empty_name_error)) else ValidationResult(true)

    }

    fun buildTournamentRulesConfigWrapper(
            entities: TournamentTypeCreatorViewModel.Entities
    ): TournamentRulesConfigWrapper {

        val tournamentType = entities.typeCreatorEntity?.tournamentType

        val wrapper = TournamentRulesConfigWrapper()

        val tournamentRulesConfig = TournamentRulesConfig()
        tournamentRulesConfig.type = TournamentType.getTournamentTypeByValue(tournamentType!!)

        tournamentRulesConfig.name = entities.commonTournamentTypeConfigEntity?.name
        tournamentRulesConfig.description = entities.commonTournamentTypeConfigEntity?.description

        wrapper.tournamentRulesConfig = tournamentRulesConfig

        if (tournamentType == TournamentType.League.value) {

            val tournamentLeagueRulesConfig = TournamentLeagueRulesConfig()
            tournamentLeagueRulesConfig.pointsForWin = entities.leagueConfigSummaryEntity!!.pointsForWin
            tournamentLeagueRulesConfig.pointsForDraw = entities.leagueConfigSummaryEntity!!.pointsForDraw
            tournamentLeagueRulesConfig.pointsForLoss = entities.configLeaguePointsEntity!!.pointsForLoss
            tournamentLeagueRulesConfig.numberOfGames = entities.leagueConfigSummaryEntity!!.numberOfGames
            tournamentLeagueRulesConfig.higherPlacePriority = entities.leagueConfigSummaryEntity!!.priority

            wrapper.tournamentLeagueRulesConfig = listOf(tournamentLeagueRulesConfig)

        } else if (tournamentType == TournamentType.Cup.value) {

            val tournamentCupRulesConfig = TournamentCupRulesConfig()
            tournamentCupRulesConfig.numberOfRegularGames = entities.cupConfigSummaryEntity!!.numberOfGames
            tournamentCupRulesConfig.numberOfFinalGames = entities.cupConfigSummaryEntity!!.numberOfFinalGames

            wrapper.tournamentCupRulesConfig = listOf(tournamentCupRulesConfig)
        }

        return wrapper
    }
}
