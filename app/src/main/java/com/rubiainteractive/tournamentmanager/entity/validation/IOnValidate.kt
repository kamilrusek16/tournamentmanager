package com.rubiainteractive.tournamentmanager.entity.validation

interface IOnValidate {
    fun onValidate(vr: ValidationResult)
}
