package com.rubiainteractive.tournamentmanager.entity.validation

class ValidationResult {

    var isValid: Boolean = false
    var message: String? = null

    constructor(valid: Boolean) {
        this.isValid = valid
    }

    constructor(valid: Boolean, message: String) {
        this.isValid = valid
        this.message = message
    }
}
