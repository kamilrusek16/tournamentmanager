package com.rubiainteractive.tournamentmanager.entity.viewentities

import com.rubiainteractive.apps.utils.viewsentities.BigEditTextPanelEntity
import com.rubiainteractive.tournamentmanager.entity.events.Event

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class BigEditTextPanelEntityRx : BigEditTextPanelEntity() {

    private val changeObservable = PublishSubject.create<Event<Int>>()

    val modelChanges: Observable<Event<Int>>
        get() = changeObservable

    override fun setEditTextValueTxt(editTextValueTxt: String) {
        super.setEditTextValueTxt(editTextValueTxt)

        var value = 0

        if (editTextValueTxt != "")
            value = Integer.valueOf(editTextValueTxt)

        changeObservable.onNext(Event(value))
    }

    override fun setEditTextValueInt(editTextValueInt: Int) {
        super.setEditTextValueInt(editTextValueInt)
        changeObservable.onNext(Event(editTextValueInt))
    }
}
