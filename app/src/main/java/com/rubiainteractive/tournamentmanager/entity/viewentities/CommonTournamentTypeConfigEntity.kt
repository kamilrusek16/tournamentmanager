package com.rubiainteractive.tournamentmanager.entity.viewentities

import android.databinding.BaseObservable
import android.databinding.Bindable

import com.rubiainteractive.tournamentmanager.BR

open class CommonTournamentTypeConfigEntity : BaseObservable() {

    @get:Bindable
    open var name: String? = null
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }
    @get:Bindable
    var description: String? = null
        set(description) {
            field = description
            notifyPropertyChanged(BR.description)
        }
}
