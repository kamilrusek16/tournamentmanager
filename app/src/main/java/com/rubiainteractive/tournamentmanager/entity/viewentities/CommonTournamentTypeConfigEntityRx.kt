package com.rubiainteractive.tournamentmanager.entity.viewentities

import com.rubiainteractive.tournamentmanager.entity.events.Event

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class CommonTournamentTypeConfigEntityRx : CommonTournamentTypeConfigEntity() {

    private val changeObservable = PublishSubject.create<Event<String>>()

    override var name: String?
        get() = super.name
        set(name) {
            super.name = name
            changeObservable.onNext(Event(name.orEmpty()))
        }

    val modelChanges: Observable<Event<String>>
        get() = changeObservable
}
