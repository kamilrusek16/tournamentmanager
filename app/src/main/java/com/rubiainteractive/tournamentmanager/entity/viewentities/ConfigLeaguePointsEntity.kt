package com.rubiainteractive.tournamentmanager.entity.viewentities

import android.databinding.BaseObservable
import android.databinding.Bindable

import com.rubiainteractive.tournamentmanager.BR

open class ConfigLeaguePointsEntity : BaseObservable() {

    open var pointsForWin: Int = 0
        set(pointsForWin) {
            field = pointsForWin
            notifyPropertyChanged(BR.pointsForWinTxt)
        }
    open var pointsForDraw: Int = 0
        set(pointsForDraw) {
            field = pointsForDraw
            notifyPropertyChanged(BR.pointsForDrawTxt)
        }
    open var pointsForLoss: Int = 0
        set(pointsForLoss) {
            field = pointsForLoss
            notifyPropertyChanged(BR.pointsForLossTxt)
        }

    @get:Bindable
    open var pointsForWinTxt: String? = null
        set(pointsForWinTxt) {
            field = pointsForWinTxt
            notifyPropertyChanged(BR.pointsForWinTxt)
        }
    @get:Bindable
    open var pointsForDrawTxt: String? = null
        set(pointsForDrawTxt) {
            field = pointsForDrawTxt
            notifyPropertyChanged(BR.pointsForDrawTxt)
        }
    @get:Bindable
    open var pointsForLossTxt: String? = null
        set(pointsForLossTxt) {
            field = pointsForLossTxt
            notifyPropertyChanged(BR.pointsForLossTxt)
        }
}
