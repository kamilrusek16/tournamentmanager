package com.rubiainteractive.tournamentmanager.entity.viewentities

import com.rubiainteractive.tournamentmanager.entity.events.Event

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ConfigLeaguePointsEntityRx : ConfigLeaguePointsEntity() {

    private val pointsForWinChangeObservable = PublishSubject.create<Event<Int>>()
    private val pointsForDrawChangeObservable = PublishSubject.create<Event<Int>>()
    private val pointsForLostChangeObservable = PublishSubject.create<Event<Int>>()

    override var pointsForWin: Int
        get() = super.pointsForWin
        set(pointsForWin) {
            super.pointsForWin = pointsForWin
            pointsForWinChangeObservable.onNext(Event(pointsForWin))
        }

    override var pointsForDraw: Int
        get() = super.pointsForDraw
        set(pointsForDraw) {
            super.pointsForDraw = pointsForDraw
            pointsForDrawChangeObservable.onNext(Event(pointsForDraw))
        }

    override var pointsForLoss: Int
        get() = super.pointsForLoss
        set(pointsForLoss) {
            super.pointsForLoss = pointsForLoss
            pointsForLostChangeObservable.onNext(Event(pointsForLoss))
        }

    override var pointsForWinTxt: String?
        get() = super.pointsForWinTxt
        set(pointsForWinTxt) {
            super.pointsForWinTxt = pointsForWinTxt

            var value = 0

            if (pointsForWinTxt != "")
                value = Integer.valueOf(pointsForWinTxt)

            pointsForDrawChangeObservable.onNext(Event(value))
        }

    override var pointsForDrawTxt: String?
        get() = super.pointsForDrawTxt
        set(pointsForDrawTxt) {
            super.pointsForDrawTxt = pointsForDrawTxt

            super.pointsForWinTxt = pointsForDrawTxt

            var value = 0

            if (pointsForDrawTxt != "")
                value = Integer.valueOf(pointsForDrawTxt)

            pointsForDrawChangeObservable.onNext(Event(value))
        }

    override var pointsForLossTxt: String?
        get() = super.pointsForLossTxt
        set(pointsForLossTxt) {
            super.pointsForLossTxt = pointsForLossTxt

            super.pointsForWinTxt = pointsForLossTxt

            var value = 0

            if (pointsForLossTxt != "")
                value = Integer.valueOf(pointsForLossTxt)

            pointsForLostChangeObservable.onNext(Event(value))
        }

    val pointsForWinChanges: Observable<Event<Int>>
        get() = pointsForWinChangeObservable

    val pointsForDrawChanges: Observable<Event<Int>>
        get() = pointsForDrawChangeObservable

    val pointsForLostChanges: Observable<Event<Int>>
        get() = pointsForLostChangeObservable
}
