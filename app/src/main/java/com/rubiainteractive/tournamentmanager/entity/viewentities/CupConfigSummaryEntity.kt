package com.rubiainteractive.tournamentmanager.entity.viewentities

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Color

import com.rubiainteractive.apps.commonutils.utils.StringUtils
import com.rubiainteractive.tournamentmanager.BR
import com.rubiainteractive.tournamentmanager.R

class CupConfigSummaryEntity(private val context: Context) : BaseObservable() {

    @get:Bindable
    var numberOfGames: Int = 0
        set(numberOfGames) {
            field = numberOfGames
            notifyPropertyChanged(BR.numberOfGames)
        }
    @get:Bindable
    var numberOfFinalGames: Int = 0
        set(numberOfFinalGames) {
            field = numberOfFinalGames
            notifyPropertyChanged(BR.numberOfFinalGames)
        }
    private var name: String? = null

    val nameTxtColor: Int
        @Bindable
        get() = if (StringUtils.isNullOrEmpty(name)) Color.RED else Color.WHITE

    @Bindable
    fun getName(): String {
        return if (StringUtils.isNullOrEmpty(name))
            context.resources.getString(R.string.name_c)
        else
            context.resources.getString(R.string.name_c) + " " + name
    }

    fun setName(name: String) {
        this.name = name
        notifyPropertyChanged(BR.name)
    }
}
