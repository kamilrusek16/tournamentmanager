package com.rubiainteractive.tournamentmanager.entity.viewentities

import com.rubiainteractive.apps.utils.viewsentities.DescWithIconPanelEntity
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority

import io.reactivex.subjects.PublishSubject

class DescWithIconPanelEntityRx : DescWithIconPanelEntity() {

    val modelChanges = PublishSubject.create<HigherPlacePriorityEvent>()!!

    init {

        addOptionOneChooseConsumer { isSelected ->
            if (isSelected!!)
                modelChanges.onNext(HigherPlacePriorityEvent(HigherPlacePriority.GoalsDifference))
        }

        addOptionTwoChooseConsumer { isSelected ->
            if (isSelected!!)
                modelChanges.onNext(HigherPlacePriorityEvent(HigherPlacePriority.SmallTable))
        }
    }

    class HigherPlacePriorityEvent internal constructor(var value: HigherPlacePriority)
}
