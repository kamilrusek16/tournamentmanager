package com.rubiainteractive.tournamentmanager.entity.viewentities

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Color

import com.rubiainteractive.apps.commonutils.utils.StringUtils
import com.rubiainteractive.tournamentmanager.BR
import com.rubiainteractive.tournamentmanager.R

import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.HigherPlacePriority

class LeagueConfigSummaryEntity(private val context: Context) : BaseObservable() {

    @get:Bindable
    var numberOfGames: Int = 0
        set(numberOfGames) {
            field = numberOfGames
            notifyPropertyChanged(BR.numberOfGames)
        }
    @get:Bindable
    var pointsForWin: Int = 0
        set(pointsForWin) {
            field = pointsForWin
            notifyPropertyChanged(BR.pointsForWin)
        }
    @get:Bindable
    var pointsForDraw: Int = 0
        set(pointsForDraw) {
            field = pointsForDraw
            notifyPropertyChanged(BR.pointsForDraw)
        }
    @get:Bindable
    var pointsForLost: Int = 0
    private val priorityImageId: Int = 0
    var priority: HigherPlacePriority? = null
        set(priority) {
            field = priority
            notifyPropertyChanged(BR.priorityImageId)
            notifyPropertyChanged(BR.priorityTxt)
        }
    private val priorityTxt: String? = null
    private var name: String? = null

    val nameTxtColor: Int
        @Bindable
        get() = if (StringUtils.isNullOrEmpty(name)) Color.RED else Color.WHITE

    @Bindable
    fun getPriorityImageId(): Int {
        return if (this.priority === HigherPlacePriority.GoalsDifference)
            R.drawable.goals
        else
            R.drawable.ic_small_table
    }

    @Bindable
    fun getPriorityTxt(): String {
        return if (this.priority === HigherPlacePriority.GoalsDifference)
            context.resources.getString(R.string.goals)
        else
            context.resources.getString(R.string.small_table)
    }

    @Bindable
    fun getName(): String {
        return if (StringUtils.isNullOrEmpty(name))
            context.resources.getString(R.string.name_c)
        else
            context.resources.getString(R.string.name_c) + " " + name
    }

    fun setName(name: String) {
        this.name = name
        notifyPropertyChanged(BR.name)
    }
}
