package com.rubiainteractive.tournamentmanager.entity.viewentities

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View

import com.anadeainc.rxbus.BusProvider
import com.rubiainteractive.tournamentmanager.BR
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType
import com.rubiainteractive.tournamentmanager.entity.events.ChangeTypeEvent

class TypeCreatorEntity : BaseObservable() {

    var tournamentType = -1
        set(tournamentType) {
            field = tournamentType
            refreshButtons()

            BusProvider.getInstance().post(ChangeTypeEvent(tournamentType))
        }
    var page: Int = 0
        set(page) {
            field = page
            notifyPropertyChanged(BR.exitVisibility)
            refreshButtons()
        }

    val exitVisibility: Int
        @Bindable
        get() = if (this.page == 0) View.VISIBLE else View.GONE

    val finishVisibility: Int
        @Bindable
        get() = if (this.page == allPagesCount - 1 && this.tournamentType != -1) View.VISIBLE else View.GONE

    val backVisibility: Int
        @Bindable
        get() = if (this.page == 0) View.GONE else View.VISIBLE

    val nextVisibility: Int
        @Bindable
        get() = if (this.page == allPagesCount - 1) View.GONE else View.VISIBLE

    val allPagesCount: Int
        get() {

            if (this.tournamentType == TournamentType.League.value)
                return PAGES_COUNT_LEAGUE
            return if (this.tournamentType == TournamentType.Cup.value) PAGES_COUNT_CUP else PAGES_COUNT_INIT

        }

    private fun refreshButtons() {

        notifyPropertyChanged(BR.exitVisibility)
        notifyPropertyChanged(BR.finishVisibility)
        notifyPropertyChanged(BR.backVisibility)
        notifyPropertyChanged(BR.nextVisibility)
    }

    companion object {

        const val PAGES_COUNT_INIT = 3
        const val PAGES_COUNT_LEAGUE = 7
        const val PAGES_COUNT_CUP = 6
    }
}
