package com.rubiainteractive.tournamentmanager.viewmodels

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent

import com.rubiainteractive.tournamentmanager.domain.interactors.launch.GetActiveTournamentsUseCase
import com.rubiainteractive.tournamentmanager.domain.interactors.launch.IsAnyHistoricalTournamentUseCase
import com.rubiainteractive.tournamentmanager.entity.dbentities.Tournament
import com.rubiainteractive.tournamentmanager.views.activities.NewTournamentActivity
import com.rubiainteractive.tournamentmanager.views.activities.TournamentTypeCreatorActivity

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LaunchViewModel(private val getActiveTournamentsUseCase: GetActiveTournamentsUseCase,
                      private val isAnyHistoricalTournamentUseCase: IsAnyHistoricalTournamentUseCase) : ViewModel() {

    private val disposables = CompositeDisposable()

    var isLoadButtonVisible: MutableLiveData<Boolean>? = null
        private set
    var isHistoryButtonVisible: MutableLiveData<Boolean>? = null
        private set

    private val activeTournamentList: MutableLiveData<List<Tournament>>? = null

    init {

        loadTournaments()
        initVisibilityFields()
    }

    override fun onCleared() {
        disposables.clear()
    }

    private fun initVisibilityFields() {

        isLoadButtonVisible = MutableLiveData()
        isLoadButtonVisible!!.value = false
        isHistoryButtonVisible = MutableLiveData()
        isHistoryButtonVisible!!.value = false
    }

    fun getActiveTournamentList(): MutableLiveData<List<Tournament>>? {

        if (activeTournamentList == null) {
            loadActiveTournaments()
        }

        return activeTournamentList
    }

    private fun loadTournaments() {
        loadActiveTournaments()
        loadOneHistoryTournament()
    }

    @SuppressLint("CheckResult")
    private fun loadActiveTournaments() {
        getActiveTournamentsUseCase
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { tournaments ->
                    this.activeTournamentList!!.value = tournaments
                    isLoadButtonVisible!!.setValue(tournaments.size > 0)
                }
    }

    @SuppressLint("CheckResult")
    private fun loadOneHistoryTournament() {

        isAnyHistoricalTournamentUseCase
                .execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { aBoolean ->
                    isHistoryButtonVisible!!.value = aBoolean
                }
    }

    fun startNewTournamentActivity(activity: Activity) {
        val intent = Intent(activity, NewTournamentActivity::class.java)
        activity.startActivity(intent)
    }

    fun startConfigActivity(activity: Activity) {
        val intent = Intent(activity, TournamentTypeCreatorActivity::class.java)
        activity.startActivity(intent)
    }
}
