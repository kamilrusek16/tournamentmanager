package com.rubiainteractive.tournamentmanager.viewmodels

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.databinding.Observable
import android.net.Uri
import android.widget.ArrayAdapter
import com.android.databinding.library.baseAdapters.BR
import com.rubiainteractive.apps.commonutils.utils.files.FileHandler
import com.rubiainteractive.apps.commonutils.utils.rx.CompositeDisposableUtil
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.domain.interactors.config.GetTournamentRulesUseCase
import com.rubiainteractive.tournamentmanager.domain.interactors.newtournament.StoreTournamentUseCase
import com.rubiainteractive.tournamentmanager.domain.services.ImagesService
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentAllParticipants
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper
import com.rubiainteractive.tournamentmanager.entity.validation.IOnValidate
import com.rubiainteractive.tournamentmanager.entity.validation.ValidationResult
import com.rubiainteractive.tournamentmanager.views.activities.NewUserActivity
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewTournamentViewModel(private val storeTournamentUseCase: StoreTournamentUseCase,
                             private val getTournamentRulesUseCase: GetTournamentRulesUseCase) : ViewModel() {

    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_IMAGE_LOAD = 2
        const val REQUEST_NEW_PARTICIPANT = 3
        const val MAX_TEAMS_NUMBER = 16
    }

    private val compositeDisposable = CompositeDisposable()
    val tournamentImage = MutableLiveData<Uri>()
    val tournamentAllParticipants = TournamentAllParticipants()

    private val outputImageFile: FileHandler = FileHandler()

    val numberOfParticipantsSummary: String
        get() = tournamentAllParticipants.participantsList!!.size.toString() +
                "/" +
                tournamentAllParticipants.getTournament()!!.teamsCount

    fun dispatchTakePictureIntent(activity: Activity) {

        val i = ImagesService().getDispatchTakePictureIntent(outputImageFile)

        if (i.resolveActivity(activity.packageManager) != null) {
            activity.startActivityForResult(i, REQUEST_IMAGE_CAPTURE)
        }
    }

    fun loadPicture(activity: Activity) {

        val i = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        if (i.resolveActivity(activity.packageManager) != null) {
            activity.startActivityForResult(i, REQUEST_IMAGE_LOAD)
        }
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        if (resultCode == RESULT_OK) {

            when (requestCode) {
                REQUEST_IMAGE_CAPTURE -> this.tournamentImage.setValue(Uri.fromFile(outputImageFile.file))
                REQUEST_IMAGE_LOAD -> this.tournamentImage.setValue(data.data)
                REQUEST_NEW_PARTICIPANT ->

                    addParticipant(
                            data.extras.getSerializable(Participant::class.java.simpleName) as Participant
                    )
            }
        }
    }

    fun confirmCreateNewTournament() {
        storeTournamentUseCase.execute(tournamentAllParticipants)
    }

    fun startNewParticipantActivity(activity: Activity) {
        val intent = Intent(activity, NewUserActivity::class.java)
        activity.startActivityForResult(intent, REQUEST_NEW_PARTICIPANT)
    }

    fun setValidateTournamentTeamNumberCallback(context: Context,
                                                onValidateCallback: IOnValidate) {

        this.tournamentAllParticipants.getTournament()!!
                .addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
                    override fun onPropertyChanged(sender: Observable, propertyId: Int) {

                        if (propertyId == BR.teamsCount)
                            onValidateCallback.onValidate(validateNumberOfTeams(context))
                    }
                })
    }

    fun shouldHideAddButton(): Boolean {
        return tournamentAllParticipants.participantsList!!.size >= tournamentAllParticipants.getTournament()!!.teamsCount
    }

    private fun validateNumberOfTeams(context: Context): ValidationResult {

        return if (tournamentAllParticipants.participantsList!!.size > tournamentAllParticipants.getTournament()!!.teamsCount) {

            ValidationResult(false, context.resources.getString(R.string.too_many_participants))

        } else {

            if (this.tournamentAllParticipants.getTournament()!!.teamsCount <= 0)
                ValidationResult(false, context.resources.getString(R.string.no_of_teams_not_filled))
            else if (this.tournamentAllParticipants.getTournament()!!.teamsCount > MAX_TEAMS_NUMBER)
                ValidationResult(false, context.resources
                        .getString(R.string.max_number_of_teams_reached, MAX_TEAMS_NUMBER))
            else
                ValidationResult(true)
        }
    }

    private fun addParticipant(participant: Participant) {

        participant.let {

            val participants = tournamentAllParticipants.participantsList?.toMutableList()
            participants?.add(it)
            tournamentAllParticipants.participants.postValue(participants)
        }
    }

    fun setTournamentRulesConfigs(rulesAdapter: ArrayAdapter<TournamentRulesConfigWrapper>) {

        rulesAdapter.clear()

        CompositeDisposableUtil.add(
                compositeDisposable,
                Single.fromCallable {
                    getTournamentRulesUseCase.execute(
                            tournamentAllParticipants.getTournament()!!.tournamentType!!.value
                    )
                }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { _ ->
                            //TODO add Timber, show dialog and log error
                        }
                        .subscribe { tournamentRulesConfigs ->
                            rulesAdapter.clear()
                            rulesAdapter.addAll(tournamentRulesConfigs)
                        }
        )
    }

    override fun onCleared() {
        super.onCleared()
        CompositeDisposableUtil.dispose(compositeDisposable)
    }
}
