package com.rubiainteractive.tournamentmanager.viewmodels

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.os.Bundle

import com.rubiainteractive.apps.commonutils.utils.StringUtils
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.domain.interactors.newtournament.GetAllParticipantsUseCase
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant
import com.rubiainteractive.tournamentmanager.entity.validation.ValidationResult

class NewUserViewModel(private val getAllParticipantsUseCase: GetAllParticipantsUseCase) : ViewModel() {

    val participant: Participant = Participant()

    fun isParticipantValid(context: Context): ValidationResult {

        return if (StringUtils.isNullOrEmpty(participant.name))
            ValidationResult(false, context.resources.getString(R.string.empty_participant_name))
        else
            ValidationResult(true)
    }

    fun saveParticipant(activity: Activity) {

        val returnData = Intent()

        val bundle = Bundle()
        bundle.putSerializable(Participant::class.java.simpleName, participant)

        returnData.putExtras(bundle)

        activity.setResult(Activity.RESULT_OK, returnData)
    }
}
