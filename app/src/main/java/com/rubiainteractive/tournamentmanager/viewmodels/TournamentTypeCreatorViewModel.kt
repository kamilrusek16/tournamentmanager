package com.rubiainteractive.tournamentmanager.viewmodels

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.widget.Toast
import com.anadeainc.rxbus.Bus
import com.anadeainc.rxbus.BusProvider
import com.anadeainc.rxbus.Subscribe
import com.rubiainteractive.apps.commonutils.utils.functional.Consumer
import com.rubiainteractive.apps.commonutils.utils.rx.CompositeDisposableUtil
import com.rubiainteractive.apps.utils.viewsentities.BigEditTextPanelEntity
import com.rubiainteractive.apps.utils.viewsentities.DescWithIconPanelEntity
import com.rubiainteractive.apps.utils.viewsentities.InfoWithIconPanelEntity
import com.rubiainteractive.tournamentmanager.domain.interactors.config.StoreTournamentRulesUseCase
import com.rubiainteractive.tournamentmanager.entity.events.ChangeTypeEvent
import com.rubiainteractive.tournamentmanager.entity.helpers.TournamentTypeEntitiesTransformer
import com.rubiainteractive.tournamentmanager.entity.viewentities.*
import com.rubiainteractive.tournamentmanager.views.activities.TournamentTypeCreatorActivity
import io.reactivex.disposables.CompositeDisposable

class TournamentTypeCreatorViewModel(val entities: Entities, private val storeTournamentRulesUseCase: StoreTournamentRulesUseCase) : ViewModel() {

    private val eventBus: Bus = BusProvider.getInstance()
    private var onChangeTypeEventConsumer: Consumer<ChangeTypeEvent>? = null
    private val tournamentTypeEntitiesTransformer: TournamentTypeEntitiesTransformer = TournamentTypeEntitiesTransformer()

    val typeCreatorEntity: TypeCreatorEntity?
        get() = entities.getTypeCreatorEntity()

    init {

        eventBus.register(this)
    }

    fun setOnChangeTypeEventConsumer(onChangeTypeEventConsumer: Consumer<ChangeTypeEvent>) {
        this.onChangeTypeEventConsumer = onChangeTypeEventConsumer
    }

    fun save(tournamentTypeCreatorActivity: TournamentTypeCreatorActivity) {

        if (validate(tournamentTypeCreatorActivity)) {

            storeTournamentRulesUseCase.execute(
                    tournamentTypeEntitiesTransformer.buildTournamentRulesConfigWrapper(entities)
            )
            tournamentTypeCreatorActivity.finish()
        }
    }

    private fun validate(context: Context): Boolean {

        val vr = tournamentTypeEntitiesTransformer.validateModel(context, entities)

        if (!vr.isValid) {
            Toast.makeText(context, vr.message, Toast.LENGTH_LONG).show()
            return false
        }

        return true
    }

    @Subscribe
    fun onEvent(event: ChangeTypeEvent) {
        onChangeTypeEventConsumer!!.accept(event)
    }

    override fun onCleared() {

        eventBus.unregister(this)
        storeTournamentRulesUseCase.dispose()
        CompositeDisposableUtil.dispose(entities.disposables)

        super.onCleared()
    }

    class Entities {

        internal var disposables: CompositeDisposable? = null

        internal var commonTournamentTypeConfigEntity: CommonTournamentTypeConfigEntity? = null
        private var infoWithIconPanelEntity: InfoWithIconPanelEntity? = null
        private var descWithIconPanelEntity: DescWithIconPanelEntity? = null
        private var bigEditTextPanelEntityLeague3: BigEditTextPanelEntity? = null
        private var bigEditTextPanelEntityCup3: BigEditTextPanelEntity? = null
        private var bigEditTextPanelEntityCup4: BigEditTextPanelEntity? = null
        internal var cupConfigSummaryEntity: CupConfigSummaryEntity? = null
        internal var configLeaguePointsEntity: ConfigLeaguePointsEntity? = null
        private var leaguePriorityEntity: DescWithIconPanelEntity? = null
        internal var leagueConfigSummaryEntity: LeagueConfigSummaryEntity? = null

        internal var typeCreatorEntity: TypeCreatorEntity? = null

        fun getCommonTournamentTypeConfigEntity(): CommonTournamentTypeConfigEntity? {
            return commonTournamentTypeConfigEntity
        }

        fun setCommonTournamentTypeConfigEntity(commonTournamentTypeConfigEntity: CommonTournamentTypeConfigEntity): Entities {
            this.commonTournamentTypeConfigEntity = commonTournamentTypeConfigEntity
            return this
        }

        fun getInfoWithIconPanelEntity(): InfoWithIconPanelEntity? {
            return infoWithIconPanelEntity
        }

        fun setInfoWithIconPanelEntity(infoWithIconPanelEntity: InfoWithIconPanelEntity): Entities {
            this.infoWithIconPanelEntity = infoWithIconPanelEntity
            return this
        }

        fun getDescWithIconPanelEntity(): DescWithIconPanelEntity? {
            return descWithIconPanelEntity
        }

        fun setDescWithIconPanelEntity(descWithIconPanelEntity: DescWithIconPanelEntity): Entities {
            this.descWithIconPanelEntity = descWithIconPanelEntity
            return this
        }

        fun getBigEditTextPanelEntityLeague3(): BigEditTextPanelEntity? {
            return bigEditTextPanelEntityLeague3
        }

        fun setBigEditTextPanelEntityLeague3(bigEditTextPanelEntityLeague3: BigEditTextPanelEntity): Entities {
            this.bigEditTextPanelEntityLeague3 = bigEditTextPanelEntityLeague3
            return this
        }

        fun getBigEditTextPanelEntityCup3(): BigEditTextPanelEntity? {
            return bigEditTextPanelEntityCup3
        }

        fun setBigEditTextPanelEntityCup3(bigEditTextPanelEntityCup3: BigEditTextPanelEntity): Entities {
            this.bigEditTextPanelEntityCup3 = bigEditTextPanelEntityCup3
            return this
        }

        fun getBigEditTextPanelEntityCup4(): BigEditTextPanelEntity? {
            return bigEditTextPanelEntityCup4
        }

        fun setBigEditTextPanelEntityCup4(bigEditTextPanelEntityCup4: BigEditTextPanelEntity): Entities {
            this.bigEditTextPanelEntityCup4 = bigEditTextPanelEntityCup4
            return this
        }

        fun getTypeCreatorEntity(): TypeCreatorEntity? {
            return typeCreatorEntity
        }

        fun setTypeCreatorEntity(typeCreatorEntity: TypeCreatorEntity): Entities {
            this.typeCreatorEntity = typeCreatorEntity
            return this
        }

        fun getCupConfigSummaryEntity(): CupConfigSummaryEntity? {
            return cupConfigSummaryEntity
        }

        fun setCupConfigSummaryEntity(cupConfigSummaryEntity: CupConfigSummaryEntity): Entities {
            this.cupConfigSummaryEntity = cupConfigSummaryEntity
            return this
        }

        fun getConfigLeaguePointsEntity(): ConfigLeaguePointsEntity? {
            return configLeaguePointsEntity
        }

        fun setConfigLeaguePointsEntity(configLeaguePointsEntity: ConfigLeaguePointsEntity): Entities {
            this.configLeaguePointsEntity = configLeaguePointsEntity
            return this
        }

        fun getLeaguePriorityEntity(): DescWithIconPanelEntity? {
            return leaguePriorityEntity
        }

        fun setLeaguePriorityEntity(leaguePriorityEntity: DescWithIconPanelEntity): Entities {
            this.leaguePriorityEntity = leaguePriorityEntity
            return this
        }

        fun getLeagueConfigSummaryEntity(): LeagueConfigSummaryEntity? {
            return leagueConfigSummaryEntity
        }

        fun setLeagueConfigSummaryEntity(leagueConfigSummaryEntity: LeagueConfigSummaryEntity): Entities {
            this.leagueConfigSummaryEntity = leagueConfigSummaryEntity
            return this
        }

        fun getDisposables(): CompositeDisposable? {
            return disposables
        }

        fun setDisposables(disposables: CompositeDisposable): Entities {
            this.disposables = disposables
            return this
        }
    }
}
