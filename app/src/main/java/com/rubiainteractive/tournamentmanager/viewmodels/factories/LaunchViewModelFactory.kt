package com.rubiainteractive.tournamentmanager.viewmodels.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.rubiainteractive.tournamentmanager.domain.interactors.launch.GetActiveTournamentsUseCase
import com.rubiainteractive.tournamentmanager.domain.interactors.launch.IsAnyHistoricalTournamentUseCase
import com.rubiainteractive.tournamentmanager.viewmodels.LaunchViewModel

class LaunchViewModelFactory(val getActiveTournamentsUseCase: GetActiveTournamentsUseCase,
                             private val isAnyHistoricalTournamentUseCase: IsAnyHistoricalTournamentUseCase) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(LaunchViewModel::class.java)) {
            return LaunchViewModel(getActiveTournamentsUseCase, isAnyHistoricalTournamentUseCase) as T
        }

        throw IllegalArgumentException("Wrong View model class")
    }
}
