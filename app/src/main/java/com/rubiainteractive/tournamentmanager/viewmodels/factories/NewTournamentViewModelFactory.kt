package com.rubiainteractive.tournamentmanager.viewmodels.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.rubiainteractive.tournamentmanager.domain.interactors.config.GetTournamentRulesUseCase
import com.rubiainteractive.tournamentmanager.domain.interactors.newtournament.StoreTournamentUseCase
import com.rubiainteractive.tournamentmanager.viewmodels.NewTournamentViewModel

import javax.inject.Inject

class NewTournamentViewModelFactory @Inject
constructor(private val storeTournamentUseCase: StoreTournamentUseCase,
            private val getTournamentRulesUseCase: GetTournamentRulesUseCase) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(NewTournamentViewModel::class.java))
            return NewTournamentViewModel(storeTournamentUseCase, getTournamentRulesUseCase) as T

        throw IllegalArgumentException("Wrong View model class")
    }
}
