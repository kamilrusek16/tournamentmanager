package com.rubiainteractive.tournamentmanager.viewmodels.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.rubiainteractive.tournamentmanager.domain.interactors.newtournament.GetAllParticipantsUseCase
import com.rubiainteractive.tournamentmanager.viewmodels.NewUserViewModel

import javax.inject.Inject

class NewUserViewModelFactory @Inject
internal constructor(private val getAllParticipantsUseCase: GetAllParticipantsUseCase) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(NewUserViewModel::class.java))
            return NewUserViewModel(getAllParticipantsUseCase) as T

        throw IllegalArgumentException("Wrong View model class")
    }
}
