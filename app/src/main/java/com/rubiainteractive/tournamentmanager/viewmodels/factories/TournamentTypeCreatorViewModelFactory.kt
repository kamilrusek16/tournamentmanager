package com.rubiainteractive.tournamentmanager.viewmodels.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.rubiainteractive.tournamentmanager.domain.interactors.config.StoreTournamentRulesUseCase
import com.rubiainteractive.tournamentmanager.viewmodels.TournamentTypeCreatorViewModel

import javax.inject.Inject

class TournamentTypeCreatorViewModelFactory @Inject
constructor(private val entities: TournamentTypeCreatorViewModel.Entities,
            private val storeTournamentRulesUseCase: StoreTournamentRulesUseCase) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(TournamentTypeCreatorViewModel::class.java))
            return TournamentTypeCreatorViewModel(entities, storeTournamentRulesUseCase) as T

        throw IllegalArgumentException("Wrong View model class")
    }
}
