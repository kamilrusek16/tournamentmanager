package com.rubiainteractive.tournamentmanager.views.activities

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.viewmodels.LaunchViewModel
import com.rubiainteractive.tournamentmanager.viewmodels.factories.LaunchViewModelFactory
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var launchViewModelFactory: LaunchViewModelFactory

    private var viewModel: LaunchViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, launchViewModelFactory).get(LaunchViewModel::class.java)

        setButtonsVisibility()
        setButtonsOnClickListeners()
    }

    private fun setButtonsOnClickListeners() {

        btn_new.setOnClickListener {
            viewModel!!.startNewTournamentActivity(this)
        }

        iv_config.setOnClickListener {
            viewModel!!.startConfigActivity(this)
        }
    }

    private fun setButtonsVisibility() {
        btn_load.isEnabled = viewModel!!.isLoadButtonVisible!!.value!!
        btn_load.isEnabled = viewModel!!.isHistoryButtonVisible!!.value!!
    }

}
