package com.rubiainteractive.tournamentmanager.views.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.rd.animation.type.AnimationType
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.viewmodels.NewTournamentViewModel
import com.rubiainteractive.tournamentmanager.viewmodels.factories.NewTournamentViewModelFactory
import com.rubiainteractive.tournamentmanager.views.adapters.CreateTournamentPagerAdapter
import com.rubiainteractive.tournamentmanager.views.extra.DepthPageTransformer
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_new_tournament.*
import javax.inject.Inject

class NewTournamentActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var newTournamentViewModelFactory: NewTournamentViewModelFactory
    @Inject
    lateinit var createTournamentPagerAdapter: CreateTournamentPagerAdapter
    @Inject
    lateinit var depthPageTransformer: DepthPageTransformer

    private var forwardMenuItem: MenuItem? = null
    private var confirmMenuItem: MenuItem? = null

    var newTournamentViewModel: NewTournamentViewModel? = null
        private set

    private val navigationButtonListener = View.OnClickListener { _ ->

        viewPager.let {

            val currentPageIndex = it?.currentItem

            when(currentPageIndex) {
                1 -> returnToMenuDialog.show()
                else -> it?.setCurrentItem(currentPageIndex!! - 1, true)
            }
        }
    }

    private val viewPagerListener: ViewPager.OnPageChangeListener
        get() = object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        toolbar!!.setNavigationIcon(R.drawable.ic_nav_home)
                        forwardMenuItem!!.isVisible = true
                        confirmMenuItem!!.isVisible = false
                    }
                    1 -> {
                        toolbar!!.setNavigationIcon(R.drawable.ic_back)
                        forwardMenuItem!!.isVisible = true
                        confirmMenuItem!!.isVisible = false
                    }
                    2 -> {
                        toolbar!!.setNavigationIcon(R.drawable.ic_back)
                        forwardMenuItem!!.isVisible = false
                        confirmMenuItem!!.isVisible = true
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        }

    private val returnToMenuDialog: MaterialDialog.Builder
        get() = MaterialDialog.Builder(this)
                .content("")
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive { _, _ -> finish() }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_tournament)

        newTournamentViewModel = ViewModelProviders.of(this, newTournamentViewModelFactory)
                .get(NewTournamentViewModel::class.java)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar?.setNavigationIcon(R.drawable.ic_nav_home)
        toolbar?.setNavigationOnClickListener(navigationButtonListener)

        viewPager?.adapter = createTournamentPagerAdapter
        viewPager?.setPageTransformer(true, depthPageTransformer)

        viewPager?.addOnPageChangeListener(viewPagerListener)

        pageIndicatorView!!.setAnimationType(AnimationType.WORM)
        pageIndicatorView!!.setViewPager(viewPager)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.create_menu, menu)

        forwardMenuItem = menu.findItem(R.id.forward)
        confirmMenuItem = menu.findItem(R.id.check)
        confirmMenuItem!!.isVisible = false

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val menuItemId = item.itemId

        when (menuItemId) {

            R.id.forward -> {
                var currentPageIndex = viewPager!!.currentItem
                viewPager!!.setCurrentItem(++currentPageIndex, true)
            }
            R.id.check -> newTournamentViewModel!!.confirmCreateNewTournament()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        newTournamentViewModel!!.handleActivityResult(requestCode, resultCode, data)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentDispatchingAndroidInjector
    }
}
