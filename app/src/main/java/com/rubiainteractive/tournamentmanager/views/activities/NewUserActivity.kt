package com.rubiainteractive.tournamentmanager.views.activities

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.viewmodels.NewUserViewModel
import com.rubiainteractive.tournamentmanager.viewmodels.factories.NewUserViewModelFactory
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class NewUserActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var newUserViewModelFactory: NewUserViewModelFactory

    var newUserViewModel: NewUserViewModel? = null
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        newUserViewModel = ViewModelProviders.of(this, newUserViewModelFactory)
                .get(NewUserViewModel::class.java)

        setContentView(R.layout.activity_new_user)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.new_user_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val menuItemId = item.itemId

        when (menuItemId) {
            R.id.check -> {

                val vr = newUserViewModel!!.isParticipantValid(this)

                if (vr.isValid) {
                    newUserViewModel!!.saveParticipant(this)
                    finish()
                } else
                    Toast.makeText(this, vr.message, Toast.LENGTH_LONG).show()
            }
        }
        return true
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return fragmentDispatchingAndroidInjector
    }
}
