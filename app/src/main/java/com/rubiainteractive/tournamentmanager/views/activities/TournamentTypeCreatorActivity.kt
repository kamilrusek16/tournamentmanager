package com.rubiainteractive.tournamentmanager.views.activities

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import com.rubiainteractive.apps.commonutils.utils.functional.Consumer
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.databinding.ActivityTournamentTypeCreatorBinding
import com.rubiainteractive.tournamentmanager.entity.events.ChangeTypeEvent
import com.rubiainteractive.tournamentmanager.viewmodels.TournamentTypeCreatorViewModel
import com.rubiainteractive.tournamentmanager.viewmodels.factories.TournamentTypeCreatorViewModelFactory
import com.rubiainteractive.tournamentmanager.views.adapters.TournamentTypeCreatorPagerAdapter
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_tournament_type_creator.*
import javax.inject.Inject

class TournamentTypeCreatorActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: TournamentTypeCreatorViewModelFactory
    private var viewModel: TournamentTypeCreatorViewModel? = null

    private var mLocked: Boolean = false

    private var adapter: TournamentTypeCreatorPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val mBinding = DataBindingUtil.setContentView<ActivityTournamentTypeCreatorBinding>(this, R.layout.activity_tournament_type_creator)
        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(TournamentTypeCreatorViewModel::class.java)

        viewModel!!.setOnChangeTypeEventConsumer(Consumer { this.updatePageIndicatorViewCount(it) })

        mBinding.entity = viewModel!!.typeCreatorEntity

        initViewSwitcher()
        decorStatusBar()
        initNavigationButtons()
    }

    private fun decorStatusBar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
    }

    private fun initNavigationButtons() {

        btn_next.setOnClickListener { _ -> moveToNextPage() }

        btn_back.setOnClickListener { _ -> moveToPrevPage() }

        btn_exit.setOnClickListener { _ -> finish() }

        btn_finish.setOnClickListener { _ -> viewModel!!.save(this) }
    }

    private fun moveToPrevPage() {

        if (mLocked)
            return

        viewModel!!.typeCreatorEntity!!.page = viewModel!!.typeCreatorEntity!!.page - 1
        viewPager!!.setCurrentItem(viewModel!!.typeCreatorEntity!!.page, true)
    }

    private fun moveToNextPage() {

        if (mLocked)
            return

        viewModel!!.typeCreatorEntity!!.page = viewModel!!.typeCreatorEntity!!.page + 1
        viewPager!!.setCurrentItem(viewModel!!.typeCreatorEntity!!.page, true)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViewSwitcher() {

        pageIndicatorView!!.setDynamicCount(true)
        pageIndicatorView!!.setViewPager(viewPager)

        adapter = TournamentTypeCreatorPagerAdapter(this, viewModel!!.entities)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                viewModel!!.typeCreatorEntity!!.page = position
            }

            override fun onPageSelected(position: Int) {}

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun updatePageIndicatorViewCount(event: ChangeTypeEvent) {

        mLocked = event.selectedTournamentType == -1
        adapter!!.notifyDataSetChanged()
    }
}
