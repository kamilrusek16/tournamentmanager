package com.rubiainteractive.tournamentmanager.views.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.rubiainteractive.tournamentmanager.views.fragments.CreateGeneralFragment
import com.rubiainteractive.tournamentmanager.views.fragments.CreateParticipantsFragment
import com.rubiainteractive.tournamentmanager.views.fragments.CreateSummaryFragment

import javax.inject.Inject

class CreateTournamentPagerAdapter @Inject
constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {

        when (position) {
            0 -> return CreateGeneralFragment()
            1 -> return CreateParticipantsFragment()
            2 -> return CreateSummaryFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 3
    }
}
