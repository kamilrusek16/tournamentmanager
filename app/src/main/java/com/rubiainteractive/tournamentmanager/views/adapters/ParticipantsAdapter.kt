package com.rubiainteractive.tournamentmanager.views.adapters

import android.arch.lifecycle.MutableLiveData
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant
import com.rubiainteractive.tournamentmanager.views.adapters.holders.ParticipantHolder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ParticipantsAdapter(private val mutableParticipants: MutableLiveData<List<Participant>>) : RecyclerView.Adapter<ParticipantHolder>() {
    private val participantList: MutableList<Participant> = mutableParticipants.value as MutableList<Participant>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantHolder {

        val inflater = LayoutInflater.from(parent.context)
        val rowView = inflater.inflate(R.layout.participant_row, parent, false)

        return ParticipantHolder(rowView)
    }

    override fun onBindViewHolder(holder: ParticipantHolder, position: Int) {
        holder.bindParticipant(participantList[position])
        holder.rootView.alpha = 1.0f

        holder.flRemoveContainer!!.setOnClickListener { v ->

            v.isEnabled = false

            holder.rootView.animate().alpha(0f).duration = 500

            Observable.timer(500, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { _ ->
                        participantList.removeAt(position)
                        mutableParticipants.postValue(participantList)
                    }
        }
    }

    override fun getItemCount(): Int {
        return participantList.size
    }
}
