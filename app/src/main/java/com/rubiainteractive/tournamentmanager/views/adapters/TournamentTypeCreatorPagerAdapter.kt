package com.rubiainteractive.tournamentmanager.views.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rubiainteractive.apps.utils.views.BigEditTextPanelView
import com.rubiainteractive.apps.utils.views.DescWithIconPanelView
import com.rubiainteractive.apps.utils.views.InfoWithIconPanelView
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType
import com.rubiainteractive.tournamentmanager.entity.viewentities.TypeCreatorEntity
import com.rubiainteractive.tournamentmanager.viewmodels.TournamentTypeCreatorViewModel
import com.rubiainteractive.tournamentmanager.views.activities.TournamentTypeCreatorActivity
import com.rubiainteractive.tournamentmanager.views.custom.CommonTournamentTypeConfigView
import com.rubiainteractive.tournamentmanager.views.custom.ConfigLeaguePointsView
import com.rubiainteractive.tournamentmanager.views.custom.CupConfigSummaryView
import com.rubiainteractive.tournamentmanager.views.custom.LeagueConfigSummaryView

class TournamentTypeCreatorPagerAdapter(private val mContext: Context, private val entities: TournamentTypeCreatorViewModel.Entities) : PagerAdapter() {

    private var infoView: InfoWithIconPanelView? = null
    private var commonTournamentTypeConfigView: CommonTournamentTypeConfigView? = null
    private var tournamentChooseView: DescWithIconPanelView? = null

    private var leagueNumberOfGamesView: BigEditTextPanelView? = null
    private var configLeaguePointsView: ConfigLeaguePointsView? = null
    private var leaguePriorityView: DescWithIconPanelView? = null
    private var leagueConfigSummaryView: LeagueConfigSummaryView? = null

    private var numberOfRegularGamesView: BigEditTextPanelView? = null
    private var numberOfRegularFinalView: BigEditTextPanelView? = null
    private var cupConfigSummaryView: CupConfigSummaryView? = null

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(mContext)

        return if (position > TypeCreatorEntity.PAGES_COUNT_INIT - 1) {

            if (entities.getTypeCreatorEntity()!!.tournamentType == TournamentType.League.value)
                this.instantiateLeagueItem(inflater, collection, position)!!
            else
                instantiateCupItem(inflater, collection, position)!!

        } else
            instantiateCommonItem(inflater, collection, position)!!
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return entities.getTypeCreatorEntity()!!.allPagesCount
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    private fun instantiateCommonItem(inflater: LayoutInflater, parent: ViewGroup, position: Int): ViewGroup? {

        when (position) {
            0 -> {

                if (infoView == null) {

                    infoView = inflater
                            .inflate(R.layout.info_with_icon_panel, parent, false) as InfoWithIconPanelView

                    infoView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    infoView!!.setEntity(entities.getInfoWithIconPanelEntity())
                }

                parent.addView(infoView)
                return infoView
            }

            1 -> {
                if (commonTournamentTypeConfigView == null) {

                    commonTournamentTypeConfigView = inflater
                            .inflate(R.layout.common_tournament_config, parent, false) as CommonTournamentTypeConfigView

                    commonTournamentTypeConfigView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    commonTournamentTypeConfigView!!.setEntity(entities.getCommonTournamentTypeConfigEntity()!!)
                }

                parent.addView(commonTournamentTypeConfigView)
                return commonTournamentTypeConfigView
            }
        }

        //case 2
        if (tournamentChooseView == null) {

            tournamentChooseView = inflater
                    .inflate(R.layout.desc_with_icon_panel, parent, false) as DescWithIconPanelView

            tournamentChooseView!!.initBinding(mContext as TournamentTypeCreatorActivity)
            tournamentChooseView!!.setEntity(entities.getDescWithIconPanelEntity())
        }

        parent.addView(tournamentChooseView)
        return tournamentChooseView
    }

    private fun instantiateLeagueItem(inflater: LayoutInflater, parent: ViewGroup, position: Int): ViewGroup? {

        when (position) {
            4 -> {

                if (configLeaguePointsView == null) {

                    configLeaguePointsView = inflater.inflate(R.layout.config_league_points_panel, parent, false) as ConfigLeaguePointsView

                    configLeaguePointsView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    configLeaguePointsView!!.setEntity(entities.getConfigLeaguePointsEntity()!!)
                }

                parent.addView(configLeaguePointsView)
                return configLeaguePointsView
            }
            5 -> {

                if (leaguePriorityView == null) {

                    leaguePriorityView = inflater
                            .inflate(R.layout.desc_with_icon_panel, parent, false) as DescWithIconPanelView

                    leaguePriorityView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    leaguePriorityView!!.setEntity(entities.getLeaguePriorityEntity())

                    leaguePriorityView!!.enableImage(leaguePriorityView!!.binding.ivOptionOne)
                    leaguePriorityView!!.binding.ivOptionOne.scaleX = 1.0f
                    leaguePriorityView!!.binding.ivOptionOne.scaleY = 1.0f
                }

                parent.addView(leaguePriorityView)
                return leaguePriorityView
            }
            6 -> {

                if (leagueConfigSummaryView == null) {

                    leagueConfigSummaryView = inflater
                            .inflate(R.layout.league_config_summary, parent, false) as LeagueConfigSummaryView

                    leagueConfigSummaryView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    leagueConfigSummaryView!!.setEntity(entities.getLeagueConfigSummaryEntity()!!)
                }

                parent.addView(leagueConfigSummaryView)
                return leagueConfigSummaryView
            }
        }

        //case 3
        if (leagueNumberOfGamesView == null) {

            leagueNumberOfGamesView = inflater.inflate(R.layout.big_edit_text_panel, parent, false) as BigEditTextPanelView

            leagueNumberOfGamesView!!.initBinding(mContext as TournamentTypeCreatorActivity)
            leagueNumberOfGamesView!!.setEntity(entities.getBigEditTextPanelEntityLeague3())
        }

        parent.addView(leagueNumberOfGamesView)
        return leagueNumberOfGamesView
    }

    private fun instantiateCupItem(inflater: LayoutInflater, parent: ViewGroup, position: Int): ViewGroup? {

        when (position) {
            3 -> {

                if (numberOfRegularGamesView == null) {

                    numberOfRegularGamesView = inflater.inflate(R.layout.big_edit_text_panel, parent, false) as BigEditTextPanelView

                    numberOfRegularGamesView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    numberOfRegularGamesView!!.setEntity(entities.getBigEditTextPanelEntityCup3())
                }

                parent.addView(numberOfRegularGamesView)
                return numberOfRegularGamesView
            }

            4 -> {

                if (numberOfRegularFinalView == null) {

                    numberOfRegularFinalView = inflater.inflate(R.layout.big_edit_text_panel, parent, false) as BigEditTextPanelView

                    numberOfRegularFinalView!!.initBinding(mContext as TournamentTypeCreatorActivity)
                    numberOfRegularFinalView!!.setEntity(entities.getBigEditTextPanelEntityCup4())
                }

                parent.addView(numberOfRegularFinalView)
                return numberOfRegularFinalView
            }
        }

        //case 5
        if (cupConfigSummaryView == null) {

            cupConfigSummaryView = inflater.inflate(R.layout.cup_config_summary, parent, false) as CupConfigSummaryView

            cupConfigSummaryView!!.initBinding(mContext as TournamentTypeCreatorActivity)
            cupConfigSummaryView!!.setEntity(entities.getCupConfigSummaryEntity()!!)
        }

        parent.addView(cupConfigSummaryView)
        return cupConfigSummaryView
    }
}
