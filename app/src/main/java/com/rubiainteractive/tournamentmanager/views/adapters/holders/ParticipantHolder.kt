package com.rubiainteractive.tournamentmanager.views.adapters.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.rubiainteractive.apps.commonutils.utils.views.adapter.OnInteractionListener
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.entity.dbentities.Participant

class ParticipantHolder(val rootView: View) : RecyclerView.ViewHolder(rootView) {

    private var tvParticipantName: TextView? = null

    var flRemoveContainer: FrameLayout? = null
        internal set

    private val onInteractionListener: OnInteractionListener? = null

    init {
        tvParticipantName = rootView.findViewById(R.id.tv_participant_name)
        flRemoveContainer = rootView.findViewById(R.id.fl_remove_container)
    }

    fun bindParticipant(participant: Participant) {
        tvParticipantName!!.text = participant.name
    }
}
