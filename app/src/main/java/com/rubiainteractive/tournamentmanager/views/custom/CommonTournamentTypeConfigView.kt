package com.rubiainteractive.tournamentmanager.views.custom

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.rubiainteractive.apps.utils.views.IBindableView
import com.rubiainteractive.tournamentmanager.databinding.CommonTournamentConfigBinding
import com.rubiainteractive.tournamentmanager.entity.viewentities.CommonTournamentTypeConfigEntity

class CommonTournamentTypeConfigView : ConstraintLayout, IBindableView<CommonTournamentTypeConfigEntity, CommonTournamentConfigBinding> {

    private var mBinding: CommonTournamentConfigBinding? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getView(): View {
        return this
    }

    override fun getBinding(): CommonTournamentConfigBinding? {
        return mBinding
    }

    override fun setEntity(entity: CommonTournamentTypeConfigEntity) {
        mBinding?.entity = entity
    }

    override fun setBinding(mBinding: CommonTournamentConfigBinding) {
        this.mBinding = mBinding
    }
}
