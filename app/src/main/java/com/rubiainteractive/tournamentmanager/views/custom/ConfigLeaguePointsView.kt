package com.rubiainteractive.tournamentmanager.views.custom

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.rubiainteractive.apps.utils.views.IBindableView
import com.rubiainteractive.tournamentmanager.databinding.ConfigLeaguePointsPanelBinding
import com.rubiainteractive.tournamentmanager.entity.viewentities.ConfigLeaguePointsEntity

class ConfigLeaguePointsView : ConstraintLayout, IBindableView<ConfigLeaguePointsEntity, ConfigLeaguePointsPanelBinding> {

    private var mBinding: ConfigLeaguePointsPanelBinding? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getView(): View {
        return this
    }

    override fun getBinding(): ConfigLeaguePointsPanelBinding? {
        return mBinding
    }

    override fun setEntity(entity: ConfigLeaguePointsEntity) {
        mBinding?.entity = entity
    }

    override fun setBinding(mBinding: ConfigLeaguePointsPanelBinding) {
        this.mBinding = mBinding
    }
}
