package com.rubiainteractive.tournamentmanager.views.custom

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.rubiainteractive.apps.utils.views.IBindableView
import com.rubiainteractive.tournamentmanager.databinding.CupConfigSummaryBinding
import com.rubiainteractive.tournamentmanager.entity.viewentities.CupConfigSummaryEntity

class CupConfigSummaryView : ConstraintLayout, IBindableView<CupConfigSummaryEntity, CupConfigSummaryBinding> {

    private var mBinding: CupConfigSummaryBinding? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getView(): View {
        return this
    }

    override fun getBinding(): CupConfigSummaryBinding? {
        return mBinding
    }

    override fun setEntity(entity: CupConfigSummaryEntity) {
        mBinding?.entity = entity
    }

    override fun setBinding(mBinding: CupConfigSummaryBinding) {
        this.mBinding = mBinding
    }
}
