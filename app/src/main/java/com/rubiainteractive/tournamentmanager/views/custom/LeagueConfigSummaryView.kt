package com.rubiainteractive.tournamentmanager.views.custom

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.rubiainteractive.apps.utils.views.IBindableView
import com.rubiainteractive.tournamentmanager.databinding.LeagueConfigSummaryBinding
import com.rubiainteractive.tournamentmanager.entity.viewentities.LeagueConfigSummaryEntity

class LeagueConfigSummaryView : ConstraintLayout, IBindableView<LeagueConfigSummaryEntity, LeagueConfigSummaryBinding> {

    private var mBinding: LeagueConfigSummaryBinding? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getView(): View {
        return this
    }

    override fun getBinding(): LeagueConfigSummaryBinding? {
        return mBinding
    }

    override fun setEntity(entity: LeagueConfigSummaryEntity) {
        mBinding?.entity = entity
    }

    override fun setBinding(mBinding: LeagueConfigSummaryBinding) {
        this.mBinding = mBinding
    }
}
