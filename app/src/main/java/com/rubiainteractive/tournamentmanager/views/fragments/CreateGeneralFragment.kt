package com.rubiainteractive.tournamentmanager.views.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import com.rubiainteractive.apps.commonutils.utils.permissions.PermissionsConsts
import com.rubiainteractive.apps.commonutils.utils.permissions.PermissionsHelper
import com.rubiainteractive.apps.commonutils.utils.views.utils.MinMaxTextWatcher
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.databinding.CreateGeneralFragmentBinding
import com.rubiainteractive.tournamentmanager.entity.TournamentTypeWrapper
import com.rubiainteractive.tournamentmanager.entity.dbentities.enums.TournamentType
import com.rubiainteractive.tournamentmanager.entity.dbentities.relationwrappers.TournamentRulesConfigWrapper
import com.rubiainteractive.tournamentmanager.viewmodels.NewTournamentViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.create_general_fragment.*
import javax.inject.Inject

class CreateGeneralFragment @Inject
constructor() : Fragment() {

    @Inject
    lateinit var noOfTeamsAdapter: ArrayAdapter<Int>
    @Inject
    lateinit var modeAdapter: ArrayAdapter<TournamentTypeWrapper>
    @Inject
    lateinit var rulesAdapter: ArrayAdapter<TournamentRulesConfigWrapper>
    @Inject
    lateinit var permissionsHelper: PermissionsHelper

    private var iPermissionTask: PermissionsHelper.IPermissionTask? = null
    private var newTournamentViewModel: NewTournamentViewModel? = null

    private val onModeItemSelectedListener: AdapterView.OnItemSelectedListener
        get() = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                if (i < 0)
                    return

                val tournamentType = modeAdapter.getItem(i)!!.tournamentType

                when (tournamentType) {
                    TournamentType.League -> {
                        til_number_of_league_teams.visibility = View.VISIBLE
                        ll_number_of_cup_teams.visibility = View.GONE
                        newTournamentViewModel!!.tournamentAllParticipants.getTournament()!!
                                .tournamentType = TournamentType.League
                    }
                    TournamentType.Cup -> {
                        til_number_of_league_teams.visibility = View.GONE
                        ll_number_of_cup_teams.visibility = View.VISIBLE
                        newTournamentViewModel!!.tournamentAllParticipants.getTournament()!!
                                .tournamentType = TournamentType.Cup
                    }
                }

                newTournamentViewModel?.setTournamentRulesConfigs(rulesAdapter)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

                til_number_of_league_teams.visibility = View.GONE
                ll_number_of_cup_teams.visibility = View.GONE

                newTournamentViewModel!!.tournamentAllParticipants.getTournament()!!
                        .tournamentType = null

                rulesAdapter.clear()
            }
        }

    private val onRulesItemSelectedListener: AdapterView.OnItemSelectedListener
        get() = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                if (i < 0)
                    return

                newTournamentViewModel!!.tournamentAllParticipants
                        .tournamentRulesConfigWrapper = rulesAdapter.getItem(i)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
                newTournamentViewModel!!.tournamentAllParticipants.getTournament()!!.rulesConfigId = 0
            }
        }

    private val minMaxTextWatcher: MinMaxTextWatcher
        get() = object : MinMaxTextWatcher(2, 16) {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                val str = s.toString()
                val n: Int
                try {
                    n = Integer.parseInt(str)
                    if (n < min) {
                        et_number_of_league_teams.error = resources.getString(R.string.min_teams_error, min)
                    } else if (n > max) {
                        et_number_of_league_teams.error = resources.getString(R.string.max_teams_error, max)
                    }
                } catch (nfe: NumberFormatException) {
                    et_number_of_league_teams.error = resources.getString(R.string.min_teams_error, min)
                }

            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        iPermissionTask = object : PermissionsHelper.IPermissionTask {
            override fun requestPermissionRationale() {
                //getPermissionDetailsDialog().show();
            }

            override fun afterPermissionGranted() {
                showTournamentPhotoContainer()
            }
        }
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

        newTournamentViewModel = ViewModelProviders.of(activity!!).get(NewTournamentViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        section_label.text = getString(R.string.new_tournament_step, 1, resources.getString(R.string.first_step_name))

        ms_number_of_cup_teams.adapter = noOfTeamsAdapter
        ms_number_of_cup_teams.setPaddingSafe(0, 0, 0, 0)

        ms_mode.adapter = modeAdapter
        ms_mode.setPaddingSafe(0, 0, 0, 0)
        ms_mode.onItemSelectedListener = onModeItemSelectedListener

        ms_rules.adapter = rulesAdapter
        ms_rules.setPaddingSafe(0, 0, 0, 0)
        ms_rules.onItemSelectedListener = onRulesItemSelectedListener

        newTournamentViewModel!!.tournamentImage.observe(this, Observer {
            iv_image.setImageURI(null)
            iv_image.setImageURI(it)
            tv_take_photo_or_upload.visibility = View.INVISIBLE
        })

        et_number_of_league_teams.addTextChangedListener(minMaxTextWatcher)

        btn_photo.setOnClickListener {
            newTournamentViewModel!!.dispatchTakePictureIntent(activity!!)
        }

        btn_upload.setOnClickListener {
            newTournamentViewModel!!.loadPicture(activity!!)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil
                .inflate<CreateGeneralFragmentBinding>(inflater, R.layout.create_general_fragment, container, false)

        binding.tournamentWrapper = newTournamentViewModel!!.tournamentAllParticipants

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        permissionsHelper.setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissionsHelper.setPermissionRequest(PermissionsConsts.WRITE_EXTERNAL_STORAGE_REQUEST)
        permissionsHelper.handlePermission(iPermissionTask)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            PermissionsConsts.WRITE_EXTERNAL_STORAGE_REQUEST -> {

                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    iPermissionTask!!.afterPermissionGranted()
                } else {
                    hideTournamentPhotoContainer()
                }
            }
        }
    }

    private fun hideTournamentPhotoContainer() {
        ll_tournament_image_container.visibility = LinearLayout.GONE
        ll_image_buttons_container.visibility = LinearLayout.GONE
    }

    private fun showTournamentPhotoContainer() {
        ll_tournament_image_container.visibility = LinearLayout.VISIBLE
        ll_image_buttons_container.visibility = LinearLayout.VISIBLE
    }
}
