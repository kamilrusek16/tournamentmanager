package com.rubiainteractive.tournamentmanager.views.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.entity.validation.IOnValidate
import com.rubiainteractive.tournamentmanager.entity.validation.ValidationResult
import com.rubiainteractive.tournamentmanager.viewmodels.NewTournamentViewModel
import com.rubiainteractive.tournamentmanager.views.adapters.ParticipantsAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.create_participants_fragment.*
import kotlinx.android.synthetic.main.create_participants_fragment_content.*
import javax.inject.Inject

//TODO Swipe to dismiss on participant, change row to cardview
class CreateParticipantsFragment @Inject
constructor() : Fragment() {

    private var newTournamentViewModel: NewTournamentViewModel? = null

    @Inject
    lateinit var mLayoutManager: RecyclerView.LayoutManager
    @Inject
    lateinit var participantsAdapter: ParticipantsAdapter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.create_participants_fragment, container, false)

        newTournamentViewModel = ViewModelProviders.of(activity!!).get(NewTournamentViewModel::class.java)

        newTournamentViewModel!!.tournamentAllParticipants
                .participants
                .observe(this, Observer {
                    refreshParticipantNumber()
                    participantsAdapter.notifyDataSetChanged()
                })

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lvParticipantList.setHasFixedSize(true)

        mLayoutManager = LinearLayoutManager(context)
        lvParticipantList.layoutManager = mLayoutManager

        lvParticipantList.adapter = participantsAdapter

        section_label.text = getString(R.string.new_tournament_step, 2, resources.getString(R.string.second_step_name))

        setTeamsCounter()

        fab_add_participant.setOnClickListener {
            newTournamentViewModel!!.startNewParticipantActivity(activity!!)
        }
    }

    private fun setTeamsCounter() {

        newTournamentViewModel!!.setValidateTournamentTeamNumberCallback(context!!, object : IOnValidate {
            override fun onValidate(vr: ValidationResult) {

                if (vr.isValid) {
                    count_label.visibility = View.VISIBLE
                    tv_no_of_teams_not_filled.visibility = View.GONE
                    count_label.text = newTournamentViewModel!!.numberOfParticipantsSummary

                    if (!newTournamentViewModel!!.shouldHideAddButton())
                        fab_add_participant.visibility = View.VISIBLE
                    else
                        fab_add_participant.visibility = View.GONE

                } else {
                    count_label.visibility = View.GONE
                    tv_no_of_teams_not_filled.visibility = View.VISIBLE
                    tv_no_of_teams_not_filled.text = vr.message
                    fab_add_participant.visibility = View.GONE
                }
            }
        })
    }

    private fun refreshParticipantNumber() {
        count_label.text = newTournamentViewModel!!.numberOfParticipantsSummary

        if (newTournamentViewModel?.shouldHideAddButton()!!)
            fab_add_participant.visibility = View.GONE
        else {

            fab_add_participant.visibility = View.VISIBLE

            if (participantsAdapter.itemCount == newTournamentViewModel!!.tournamentAllParticipants.participantsList!!.size - 1) {

                fab_add_participant.alpha = 0.0f
                fab_add_participant.animate().alpha(1.0f).duration = 1000
            }
        }
    }
}
