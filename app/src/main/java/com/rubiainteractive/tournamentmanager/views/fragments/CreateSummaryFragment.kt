package com.rubiainteractive.tournamentmanager.views.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.databinding.CreateSummaryFragmentBinding
import kotlinx.android.synthetic.main.create_summary_fragment.*
import javax.inject.Inject

class CreateSummaryFragment @Inject
constructor() : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<CreateSummaryFragmentBinding>(inflater, R.layout.create_summary_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        section_label.text = getString(R.string.new_tournament_step, 3, resources.getString(R.string.third_step_name))
    }
}
