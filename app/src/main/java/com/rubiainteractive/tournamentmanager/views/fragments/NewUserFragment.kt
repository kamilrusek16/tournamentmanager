package com.rubiainteractive.tournamentmanager.views.fragments

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rubiainteractive.tournamentmanager.R
import com.rubiainteractive.tournamentmanager.databinding.NewUserFragmentBinding
import com.rubiainteractive.tournamentmanager.viewmodels.NewUserViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.new_user_fragment.*
import javax.inject.Inject

class NewUserFragment @Inject
constructor() : Fragment() {

    @Inject
    lateinit var newUserViewModel: NewUserViewModel

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil
                .inflate<NewUserFragmentBinding>(inflater, R.layout.new_user_fragment, container, false)

        val rootView = binding.root

        binding.participant = newUserViewModel.participant

        activity.apply {

            (this as AppCompatActivity?)?.setSupportActionBar(toolbar)
            this?.supportActionBar?.setDisplayShowTitleEnabled(false)

            toolbar.setNavigationIcon(R.drawable.ic_back)
            toolbar.setNavigationOnClickListener { activity!!.finish() }
        }

        return rootView
    }
}
