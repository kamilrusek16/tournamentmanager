package com.rubiainteractive.apps.commonutils.utils;

import android.databinding.InverseMethod;

public class StringUtils {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static String[] getStringArray(String... source) {
        return source;
    }

    public static String formatPlayerName(String firstName, String lastName) {

        if(!StringUtils.isNullOrEmpty(firstName) && !StringUtils.isNullOrEmpty(lastName))
            return firstName.charAt(0) + ". " + lastName;
        else if (StringUtils.isNullOrEmpty(firstName) && !StringUtils.isNullOrEmpty(lastName))
            return lastName;
        else if (!StringUtils.isNullOrEmpty(firstName) && StringUtils.isNullOrEmpty(lastName))
            return firstName;

        return null;
    }

    public static String getPlayerFullName(String firstName, String lastName) {

        StringBuilder stringBuilder = new StringBuilder();

        if(!isNullOrEmpty(firstName)){

            stringBuilder.append(firstName);

            if(!isNullOrEmpty(lastName))
                stringBuilder.append(" ");
        }

        if(!isNullOrEmpty(lastName))
            stringBuilder.append(lastName);

        return stringBuilder.toString();
    }

    public static String stringfyInt(int i) {

        if(i == Integer.MIN_VALUE)
            return "";

        return String.valueOf(i);
    }

    @InverseMethod("stringfyInt")
    public static int stringToInt(String str) {

        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return Integer.MIN_VALUE;
        }
    }
}
