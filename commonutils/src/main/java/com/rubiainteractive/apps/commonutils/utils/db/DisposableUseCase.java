package com.rubiainteractive.apps.commonutils.utils.db;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class DisposableUseCase {

    protected CompositeDisposable disposables = new CompositeDisposable();

    protected void add(Disposable d){
        disposables.add(d);
    }

    public void dispose() {
        disposables.dispose();
    }
}
