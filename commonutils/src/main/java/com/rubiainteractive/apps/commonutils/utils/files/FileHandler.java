package com.rubiainteractive.apps.commonutils.utils.files;

import java.io.File;
import java.io.FileNotFoundException;

public class FileHandler {

    private File file;

    public void createFile(String dirPath, String fileName) {

        File dir = new File(dirPath);

        if(!dir.exists() && !dir.mkdir())
            return;

        file = new File(dir, fileName);
    }

    public void loadFile(String path) {
        this.file = new File(path);
    }

    public File getFile() {
        return file;
    }
}
