package com.rubiainteractive.apps.commonutils.utils.functional;

public interface Consumer<T> {
    void accept(T t);
}
