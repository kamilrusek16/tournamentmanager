package com.rubiainteractive.apps.commonutils.utils.permissions;

public interface PermissionsConsts {
    int WRITE_EXTERNAL_STORAGE_REQUEST = 1;
    int READ_EXTERNAL_STORAGE_REQUEST = 2;
}
