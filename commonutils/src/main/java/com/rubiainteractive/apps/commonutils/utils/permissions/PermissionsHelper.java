package com.rubiainteractive.apps.commonutils.utils.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class PermissionsHelper {

    private int permissionRequest;
    private Activity activity;
    private String permission;

    public PermissionsHelper(String permission, Activity activity, int permissionRequest) {

        this.activity = activity;
        this.permission = permission;
        this.permissionRequest = permissionRequest;
    }

    public PermissionsHelper(Activity activity, int permissionRequest) {
        this.activity = activity;
        this.permissionRequest = permissionRequest;
    }

    public PermissionsHelper(Activity activity) {
        this.activity = activity;
    }

    public void setPermissions(String permission) {
        this.permission = permission;
    }

    public void setPermissionRequest(int permissionRequest) {
        this.permissionRequest = permissionRequest;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void handlePermission(IPermissionTask iPermissionTask) {

        if(!checkAllPermissionsGranted()) {

            if(shouldShowPermissionsRationale()) {
                iPermissionTask.requestPermissionRationale();

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionRequest);
            }

        } else {
            iPermissionTask.afterPermissionGranted();
        }
    }

    private boolean shouldShowPermissionsRationale() {
        return shouldShowSinglePermissionRationale(permission);
    }

    private boolean shouldShowSinglePermissionRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    private boolean checkAllPermissionsGranted() {
        return checkSinglePermissionGranted(permission);
    }

    private boolean checkSinglePermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public interface IPermissionTask {
        void requestPermissionRationale();
        void afterPermissionGranted();
    }
}
