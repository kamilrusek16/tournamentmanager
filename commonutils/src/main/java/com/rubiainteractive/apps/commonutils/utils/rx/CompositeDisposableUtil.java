package com.rubiainteractive.apps.commonutils.utils.rx;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java8.util.Optional;

public class CompositeDisposableUtil {

    public static void add(CompositeDisposable compositeDisposable, Disposable disposable) {
        Optional.ofNullable(compositeDisposable).ifPresent(compositeDisposable1 -> compositeDisposable1.add(disposable));
    }

    public static void dispose(CompositeDisposable compositeDisposable) {
        Optional.ofNullable(compositeDisposable).ifPresent(CompositeDisposable::dispose);
    }
}
