package com.rubiainteractive.apps.commonutils.utils.views.adapter;

public interface OnInteractionListener {
    void onClick(int position);
    void onLongClick(int position);
}
