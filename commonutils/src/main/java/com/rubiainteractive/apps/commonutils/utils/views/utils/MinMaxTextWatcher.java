package com.rubiainteractive.apps.commonutils.utils.views.utils;

import android.text.TextWatcher;

public abstract class MinMaxTextWatcher implements TextWatcher {

    protected int min, max;

    protected MinMaxTextWatcher(int min, int max) {
        super();
        this.min = min;
        this.max = max;
    }

}
