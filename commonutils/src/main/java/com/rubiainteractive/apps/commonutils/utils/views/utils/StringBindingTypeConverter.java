package com.rubiainteractive.apps.commonutils.utils.views.utils;

import android.databinding.InverseMethod;

public class StringBindingTypeConverter {

    @InverseMethod("convertIntToString")
    public static int convertStringToInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String convertIntToString(int value) {
        return String.valueOf(value);
    }
}
