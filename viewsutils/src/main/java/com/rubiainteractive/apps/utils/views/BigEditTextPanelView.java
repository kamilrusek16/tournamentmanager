package com.rubiainteractive.apps.utils.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;

import com.rubiainteractive.apps.utils.databinding.BigEditTextPanelBinding;
import com.rubiainteractive.apps.utils.viewsentities.BigEditTextPanelEntity;

import java8.util.Optional;

public class BigEditTextPanelView extends ConstraintLayout
        implements IBindableView<BigEditTextPanelEntity, BigEditTextPanelBinding>{

    private BigEditTextPanelBinding mBinding;
    private BigEditTextPanelEntity entity;

    public BigEditTextPanelView(Context context) {
        super(context);
    }

    public BigEditTextPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BigEditTextPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public BigEditTextPanelBinding getBinding() {
        return mBinding;
    }

    @Override
    public void setBinding(BigEditTextPanelBinding mBinding) {
        this.mBinding = mBinding;
    }

    @Override
    public void setEntity(BigEditTextPanelEntity entity) {

        this.entity = entity;

        Optional.ofNullable(mBinding).ifPresent(
                bigEditTextPanelBinding -> bigEditTextPanelBinding.setEntity(entity)
        );
    }

}
