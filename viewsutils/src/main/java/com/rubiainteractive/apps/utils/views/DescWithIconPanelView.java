package com.rubiainteractive.apps.utils.views;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.rubiainteractive.apps.utils.databinding.DescWithIconPanelBinding;
import com.rubiainteractive.apps.utils.viewsentities.DescWithIconPanelEntity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java8.util.Optional;

public class DescWithIconPanelView extends ConstraintLayout 
        implements IBindableView<DescWithIconPanelEntity, DescWithIconPanelBinding> {

    private DescWithIconPanelEntity entity;
    private DescWithIconPanelBinding mBinding;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public DescWithIconPanelView(Context context) {
        super(context);
    }

    public DescWithIconPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DescWithIconPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public DescWithIconPanelBinding getBinding() {
        return mBinding;
    }

    @Override
    public void setEntity(DescWithIconPanelEntity entity) {

        this.entity = entity;

        Optional.ofNullable(mBinding).ifPresent(
                descWithIconPanelBinding -> {
                    descWithIconPanelBinding.setEntity(entity);
                    getControls(descWithIconPanelBinding);
                }
        );
    }

    @Override
    public void setBinding(DescWithIconPanelBinding mBinding) {
        this.mBinding = mBinding;
    }

    private void getControls(DescWithIconPanelBinding descWithIconPanelBinding) {

        entity.addOptionOneChooseConsumer(
                isSelected -> getImageClickConsumer(descWithIconPanelBinding.ivOptionOne, isSelected)
        );

        entity.addOptionTwoChooseConsumer(
                isSelected -> getImageClickConsumer(descWithIconPanelBinding.ivOptionTwo, isSelected)
        );

        descWithIconPanelBinding.ivOptionOne.setOnClickListener(
                v -> entity.setOptionOneSelected(!entity.isOptionOneSelected())
        );

        descWithIconPanelBinding.ivOptionTwo.setOnClickListener(
                v -> entity.setOptionTwoSelected(!entity.isOptionTwoSelected())
        );

        //disable images on start view
        disableImage(descWithIconPanelBinding.ivOptionOne);
        disableImage(descWithIconPanelBinding.ivOptionTwo);
    }

    private void getImageClickConsumer(ImageView targetImageView, boolean isSelected) {

        if (isSelected)
            selectImageAndAnimate(targetImageView);
        else
            unselectImageAndAnimate(targetImageView);
    }

    private void selectImageAndAnimate(ImageView targetImage) {

        float selectedScaleImage = 1.0f;

        targetImage.setEnabled(false);

        targetImage
                .animate()
                .scaleX(selectedScaleImage)
                .scaleY(selectedScaleImage)
                .withStartAction(() -> enableImage(targetImage))
                .setDuration(500);

        //disable fast multiple clicks
        compositeDisposable.add(
                Observable.timer(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> targetImage.setEnabled(true))
        );
    }

    private void unselectImageAndAnimate(ImageView targetImage) {

        float unselectedScaleImage = 0.8f;

        targetImage.setEnabled(false);

        targetImage
                .animate()
                .scaleX(unselectedScaleImage)
                .scaleY(unselectedScaleImage)
                .withStartAction(() -> disableImage(targetImage))
                .setDuration(500);

        //disable fast multiple clicks
        compositeDisposable.add(
                Observable.timer(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> targetImage.setEnabled(true))
        );
    }

    public void enableImage(ImageView imageView) {
        imageView.setColorFilter(null); // reset color filter
    }

    public void disableImage(ImageView imageView) {

        final ColorMatrix grayscaleMatrix = new ColorMatrix();
        grayscaleMatrix.setSaturation(0);

        final ColorMatrixColorFilter filter = new ColorMatrixColorFilter(grayscaleMatrix);
        imageView.setColorFilter(filter);
    }

    public void dispose() {
        Optional.ofNullable(compositeDisposable).ifPresent(CompositeDisposable::dispose);
    }
}
