package com.rubiainteractive.apps.utils.views;

import android.arch.lifecycle.LifecycleOwner;
import android.databinding.BaseObservable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.View;

import java8.util.Optional;

public interface IBindableView<T extends BaseObservable, V extends ViewDataBinding> {

    View getView();
    V getBinding();

    void setEntity(T entity);
    void setBinding(V mBinding);

    default void setBindingLifecycleOwner(LifecycleOwner lifecycleOwner, ViewDataBinding mBinding) {

        Optional.ofNullable(mBinding).ifPresent(
                binding -> binding.setLifecycleOwner(lifecycleOwner)
        );
    }

    default void initBinding(LifecycleOwner lifecycleOwner) {
        setBinding(DataBindingUtil.bind(getView()));
        //setBindingLifecycleOwner(lifecycleOwner, getBinding());
    }
}
