package com.rubiainteractive.apps.utils.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;

import com.rubiainteractive.apps.utils.R;
import com.rubiainteractive.apps.utils.databinding.InfoWithIconPanelBinding;
import com.rubiainteractive.apps.utils.viewsentities.InfoWithIconPanelEntity;

import java8.util.Optional;

public class InfoWithIconPanelView extends ConstraintLayout
        implements IBindableView<InfoWithIconPanelEntity, InfoWithIconPanelBinding> {

    private InfoWithIconPanelBinding mBinding;
    private InfoWithIconPanelEntity entity;

    public InfoWithIconPanelView(Context context) {
        super(context);
    }

    public InfoWithIconPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InfoWithIconPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public InfoWithIconPanelBinding getBinding() {
        return mBinding;
    }

    @Override
    public void setEntity(InfoWithIconPanelEntity entity) {

        this.entity = entity;

        Optional.ofNullable(mBinding).ifPresent(
                infoWithIconPanelBinding -> infoWithIconPanelBinding.setEntity(entity)
        );
    }

    @Override
    public void setBinding(InfoWithIconPanelBinding mBinding) {
        this.mBinding = mBinding;
    }
}
