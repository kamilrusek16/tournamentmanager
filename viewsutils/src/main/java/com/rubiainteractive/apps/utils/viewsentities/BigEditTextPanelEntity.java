package com.rubiainteractive.apps.utils.viewsentities;

import com.rubiainteractive.apps.utils.BR;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class BigEditTextPanelEntity extends BaseObservable {

    private String title;
    private String editTextValueTxt;

    private String errorMessage;

    private boolean showError;

    private int minValue = 1;
    private int maxValue = 5;

    private String minErrorMessage;
    private String maxErrorMessage;

    private int editTextValueInt;

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Bindable
    public String getEditTextValueTxt() {
        return editTextValueTxt;
    }

    public void setEditTextValueTxt(String editTextValueTxt) {

        this.editTextValueTxt = editTextValueTxt;

        if(!editTextValueTxt.isEmpty())
            this.editTextValueInt = Integer.valueOf(editTextValueTxt);
        else
            this.editTextValueInt = 0;

        if(editTextValueInt > maxValue) {

            showError = true;
            setErrorMessage(maxErrorMessage);

        } else if(editTextValueInt < minValue) {

            showError = true;
            setErrorMessage(minErrorMessage);

        } else {
            showError = false;
        }

        notifyPropertyChanged(BR.errorVisibility);
        notifyPropertyChanged(BR.editTextValueTxt);
    }

    public int getEditTextValueInt() {
        return editTextValueInt;
    }

    @Bindable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        notifyPropertyChanged(BR.errorMessage);
    }

    public void setEditTextValueInt(int editTextValueInt) {

        this.editTextValueInt = editTextValueInt;
        this.editTextValueTxt = String.valueOf(editTextValueInt);

        notifyPropertyChanged(BR.editTextValueTxt);
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public String getMinErrorMessage() {
        return minErrorMessage;
    }

    public void setMinErrorMessage(String minErrorMessage) {
        this.minErrorMessage = minErrorMessage;
    }

    public String getMaxErrorMessage() {
        return maxErrorMessage;
    }

    public void setMaxErrorMessage(String maxErrorMessage) {
        this.maxErrorMessage = maxErrorMessage;
    }

    @Bindable
    public int getErrorVisibility(){
        return !showError ? INVISIBLE : VISIBLE;
    }
}
