package com.rubiainteractive.apps.utils.viewsentities;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.rubiainteractive.apps.commonutils.utils.functional.Consumer;
import com.rubiainteractive.apps.utils.BR;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class DescWithIconPanelEntity extends BaseObservable {

    private String title;
    private String option1;
    private String option2;
    private String summary;
    private String selectedOption;

    private int selectedImage1Id;
    private int selectedImage2Id;

    private int unselectedImage1Id;
    private int unselectedImage2Id;

    private int finalSelectedImage1Id;
    private int finalSelectedImage2Id;

    private boolean optionOneSelected;
    private boolean optionTwoSelected;

    private List<Consumer<Boolean>> optionOneChooseConsumers;
    private List<Consumer<Boolean>> optionTwoChooseConsumers;

    public DescWithIconPanelEntity() {

        optionOneChooseConsumers = new ArrayList<>();
        optionTwoChooseConsumers = new ArrayList<>();
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Bindable
    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    @Bindable
    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    @Bindable
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
        notifyPropertyChanged(BR.summary);
    }

    @Bindable
    public int getSelectedImage1Id() {
        return selectedImage1Id;
    }

    public void setSelectedImage1Id(int selectedImage1Id) {
        this.selectedImage1Id = selectedImage1Id;
    }

    @Bindable
    public int getSelectedImage2Id() {
        return selectedImage2Id;
    }

    public void setSelectedImage2Id(int selectedImage2Id) {
        this.selectedImage2Id = selectedImage2Id;
    }

    @Bindable
    public int getUnselectedImage1Id() {
        return unselectedImage1Id;
    }

    public void setUnselectedImage1Id(int unselectedImage1Id) {
        this.unselectedImage1Id = unselectedImage1Id;
    }

    @Bindable
    public int getUnselectedImage2Id() {
        return unselectedImage2Id;
    }

    public void setUnselectedImage2Id(int unselectedImage2Id) {
        this.unselectedImage2Id = unselectedImage2Id;
    }

    @Bindable
    public boolean isOptionOneSelected() {
        return optionOneSelected;
    }

    public void setOptionOneSelected(boolean optionOneSelected) {

        this.optionOneSelected = optionOneSelected;

        if(optionOneSelected) {

            if(optionTwoSelected)
                setOptionTwoSelected(false);

            setSelectedOption(option1);
        }

        finalSelectedImage1Id = optionOneSelected ? selectedImage1Id : unselectedImage1Id;
        notifyPropertyChanged(BR.finalSelectedImage1Id);

        runOptionOneChooseConsumers(optionOneSelected);

        notifyPropertyChanged(BR.summaryVisibility);
    }

    @Bindable
    public boolean isOptionTwoSelected() {
        return optionTwoSelected;
    }

    public void setOptionTwoSelected(boolean optionTwoSelected) {

        this.optionTwoSelected = optionTwoSelected;

        if(optionTwoSelected) {

            if(optionOneSelected)
                setOptionOneSelected(false);

            setSelectedOption(option2);
        }

        finalSelectedImage2Id = optionTwoSelected ? selectedImage2Id : unselectedImage2Id;
        notifyPropertyChanged(BR.finalSelectedImage2Id);

        runOptionTwoChooseConsumers(optionTwoSelected);

        notifyPropertyChanged(BR.summaryVisibility);
    }

    @Bindable
    public int getFinalSelectedImage1Id() {
        return finalSelectedImage1Id;
    }

    @Bindable
    public int getFinalSelectedImage2Id() {
        return finalSelectedImage2Id;
    }

    public void setFinalSelectedImage1Id(int finalSelectedImage1Id) {
        this.finalSelectedImage1Id = finalSelectedImage1Id;
    }

    public void setFinalSelectedImage2Id(int finalSelectedImage2Id) {
        this.finalSelectedImage2Id = finalSelectedImage2Id;
    }

    public void addOptionOneChooseConsumer(Consumer<Boolean> optionOneChooseConsumer) {
        this.optionOneChooseConsumers.add(optionOneChooseConsumer);
    }

    public void addOptionTwoChooseConsumer(Consumer<Boolean> optionTwoChooseConsumer) {
        this.optionTwoChooseConsumers.add(optionTwoChooseConsumer);
    }

    private void runOptionOneChooseConsumers(boolean isSelected) {
        for(Consumer<Boolean> consumer : this.optionOneChooseConsumers) {
            consumer.accept(isSelected);
        }
    }

    private void runOptionTwoChooseConsumers(boolean isSelected) {
        for(Consumer<Boolean> consumer : this.optionTwoChooseConsumers) {
            consumer.accept(isSelected);
        }
    }

    @Bindable
    public int getSummaryVisibility(){
        return !isOptionOneSelected() && !isOptionTwoSelected() ? INVISIBLE : VISIBLE;
    }

    @Bindable
    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
        notifyPropertyChanged(BR.selectedOption);
    }
}
