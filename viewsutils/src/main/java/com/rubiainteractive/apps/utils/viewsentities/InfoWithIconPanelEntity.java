package com.rubiainteractive.apps.utils.viewsentities;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class InfoWithIconPanelEntity extends BaseObservable {

    private String info1;
    private int imageResourceId;

    @Bindable
    public String getInfo1() {
        return info1;
    }

    public void setInfo(String info1) {
        this.info1 = info1;
    }

    @Bindable
    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }
}
